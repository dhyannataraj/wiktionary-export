#!/usr/bin/perl

use strict;
use MediaWiki::API;


use URI::Escape;
use utf8;
binmode STDOUT, ':utf8';

use Wiktionary::Server;

my $server = new Wiktionary::Server(lang=>'ru');
my @list = $server->get_word_list('ru');

foreach (@list)
{
 print "$_\n";
}
