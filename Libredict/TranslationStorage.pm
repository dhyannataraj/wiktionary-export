package Libredict::TranslationStorage;

use 5.010;
use strict;
use warnings;
use Moo;
use Path::Tiny;
use Data::Dumper;

use utf8;

use experimental 'smartmatch';

has 'source_lang' => (
    is => 'rw',
);

has 'target_langs' => (
    is => 'rw',
);


has 'logger' => (
    is => 'rw',
);

has '_index' =>
(
    is => 'rw',
    default => sub{{}}

);


around 'BUILDARGS', sub {
  my ($orig, $class, %args) = @_;
  # Это только для тестов
  `rm -r storage_test`;
  `mkdir storage_test`;

  $class->$orig(%args);
};


sub add_wikipage
{
  my $self = shift;
  my $page = shift;
  my $logger = $self->logger;

  my $title = $page->title || \&default_logger;
print "---------", $self->source_lang,"\n";

  my $sec;
  eval {$sec = $page->get_section($self->source_lang)};
  if ($@)
  {
    my $error_msg = $@;
    $logger->($title ,$error_msg);
    return 0;
  }
  my @articles;
  eval {@articles = $sec->articles;};
  if ($@)
  {
    my $error_msg = $@;
    $logger->($title ,$error_msg);
    return 0;
  }

  foreach my $art (@articles)
  {
    my $all_trans;
    eval {$all_trans = $art->all_translations};
    if ($@)
    {
      my $error_msg = $@;
      $logger->($title,$error_msg);
      next;
    }
    foreach my $to_lang (keys %{$all_trans})
    {
      if (! $self->target_langs || $to_lang ~~ @{$self->target_langs})
      {

      print "===== $title ".$self->source_lang."->$to_lang\n";
        use Data::Dumper;
        print Dumper $all_trans->{$to_lang};
        print $all_trans->{$to_lang}->asString();

        $self->_save_translation($title, $self->source_lang, $to_lang, $all_trans->{$to_lang});
      } else
      {
          # FIXME add here some hook to register unused translation, if needed.
#        print "SKIPPING\n";
      }
    }
  }
}

sub escape_name
{
  my $name = shift;

  $name =~ tr/а-яА-Яa-zA-Z/_/c;  #anything that are not latin and cyrittlic letters are replaced with _
  return $name;
}

sub mk_unique_dir
{
  my $self = shift;
  my $file_name = path(shift);

  unless ($file_name->exists)
  {
    $file_name->mkpath;
print "^^^^^^^^^^^ $file_name\n";
    return $file_name;
  }
  my $base_name = $file_name;
  my $count = 0;
  while (1)
  {
    $file_name = path($base_name."_$count");
    unless ($file_name->exists)
    {
      $file_name->mkpath;
print "^^^^^^^^^^^ $file_name\n";
      return $file_name;
    }
    $count++;
  }
}


sub _save_translation
{
   my $self = shift;
   my $title = shift;
   my $from_lang = shift;
   my $to_lang = shift;
   my $data = shift;

print "`````````````````````````````````````````\n";
print Dumper( $self->{knonw_entries});
print "`````````````````````````````````````````\n";

my $tmp_dir = path("storage_test");


   $self->{knonw_entries} ||= {}; # Init with {} if have not been inited before

   $self->{knonw_entries}->{$from_lang} ||={}; # Also init;
   $self->{knonw_entries}->{$from_lang}->{$to_lang} ||= 0; # also init;

   $self->{knonw_entries}->{$from_lang}->{$to_lang}++;

  my $dir;
  my $index = $self->_index;
  if ($index->{$title})
  {
    $dir = path($index->{$title});
  } else
  {
    $dir = $self->mk_unique_dir($tmp_dir->child(escape_name($title)));
    $index->{$title} = "$dir";
  }

   $dir = $dir->child($from_lang,$to_lang);
   $dir->mkpath;


  my $file = $dir->child($self->{knonw_entries}->{$from_lang}->{$to_lang});
  $file->spew(Dumper($data));

  print "======== $dir ------",$data->form->title,"++\n";
}

sub default_logger
{
  warn join " ",  @_;
}


1;