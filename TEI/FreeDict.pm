package TEI::FreeDict;

use strict;
use utf8;

use XML::LibXML;

use TEI::FreeDict::Header;
use TEI::FreeDict::Entry;
use TEI::FreeDict::Helpers;

sub new
{
  my $class = shift;
  my $title = shift;
  
  my $self = {_entries=>[]};
  
  bless($self,$class);
  return $self;
}

sub add_entry
{
  my $self = shift;
  my $entry = shift;
  push @{$self->{_entries}}, $entry;
}

sub add_header
{
  my $self = shift;
  my @opt = @_;
  if (ref $opt[0] eq 'TEI::FreeDict::Header')
  {
    $self->{_header} = $opt[0];
  } else
  {
    $self->{_header} = new TEI::FreeDict::Header(@opt);
  }
}

sub asString
{
  my $self = shift;
  my $str = '<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" >
';
  $str .= $self->{_header}->asString if $self->{_header};
  $str .= '  <text>
    <body>
';
  foreach (@{$self->{_entries}})
  {
    $str.=$_->asString;
  }
  $str .="    </body>
  </text>
</TEI>";
  
  return $str;
}

sub load_from_xml
{
  my $self = shift;
  my $xml = shift;

  my $root = $xml->documentElement();

  my @children = $root->nonBlankChildNodes();
  my $teiHeader = shift @children;
  TEI::FreeDict::Helpers::_check_node_name_expected_or_die($teiHeader, 'teiHeader');
  my $text = shift @children;
  TEI::FreeDict::Helpers::_check_node_name_expected_or_die($text, 'text');
  TEI::FreeDict::Helpers::_check_node_does_not_exist_or_die(shift @children);

  @children = $text->nonBlankChildNodes();

  my $body = shift @children;
  TEI::FreeDict::Helpers::_check_node_name_expected_or_die($body, 'body');
  TEI::FreeDict::Helpers::_check_node_does_not_exist_or_die(shift @children);

  @children = $body->nonBlankChildNodes();

  foreach my $entry_node (@children)
  {
    TEI::FreeDict::Helpers::_check_node_name_expected_or_die($entry_node, 'entry');
    my $entry = TEI::FreeDict::Entry->new($entry_node);
  }
}
1;
