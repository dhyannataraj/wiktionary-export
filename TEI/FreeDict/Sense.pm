package TEI::FreeDict::Sense;

use TEI::FreeDict::Translation;
use TEI::FreeDict::Definition;

use strict;

sub new
{
  my $class = shift;
  my $self={_translations=>[],_definitions=>[]};
  
  bless($self,$class);
  return $self;
}

sub add_definition
{
  my $self = shift;
  my $value = shift;

  my $tr = new TEI::FreeDict::Definition($value);
  push @{$self->{_definitions}}, $tr;
}

sub add_translation
{
  my $self = shift;
  my $value = shift;

  my $tr = new TEI::FreeDict::Translation($value);
  push @{$self->{_translations}}, $tr;
}

sub translations
{
  my $self = shift;
  return @{$self->{_translations}};
}

sub definitions
{
  my $self = shift;
  return @{$self->{_definitions}};
}

sub asString
{
  my $self = shift;
  my $str = "";
  foreach (@{$self->{_translations}})
  {
    $str.= $_->asString;
  }
  foreach (@{$self->{_definitions}})
  {
    $str.= $_->asString;
  }
  $str = "        <sense>\n$str        </sense>\n";
  return $str;
}
1;