package TEI::FreeDict::Misc;

use strict;
use utf8;

use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION);
require Exporter;

@ISA = qw(Exporter);

@EXPORT = qw(
	string_XML_encode
);

@EXPORT_OK = qw(
	string_XML_encode
);


sub string_XML_encode
{
  my $str = shift;
  $str=~s/\&/\&amp;\;/g;
  $str=~s/\</\&lt\;/g;
  $str=~s/\>/\&gt\;/g;
  return $str;
}

1;
