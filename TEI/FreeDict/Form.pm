package TEI::FreeDict::Form;

use TEI::FreeDict::Sense;
use TEI::FreeDict::Misc;

use strict;

sub new
{
  my $class = shift;
  my $title = shift;
  
  my $self = {_title=>$title};
  
  bless($self,$class);
  return $self;
}

sub asString
{
  my $self = shift;
  my $str = '          <orth>'.TEI::FreeDict::Misc::string_XML_encode($self->{_title})."</orth>\n";
  $str .= '          <pron>'.TEI::FreeDict::Misc::string_XML_encode($self->pronunciation)."</pron>\n" if $self->pronunciation;
  
  $str = "        <form>\n$str        </form>\n";
  
  return $str;
}

sub title
{
  my $self = shift;
  return $self->{_title};
}

sub pronunciation
{
  my $self = shift;
  my $value = shift;

  $self->{_pronunciation} = $value if defined $value;
  return $self->{_pronunciation};
}
1;