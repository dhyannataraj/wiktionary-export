package TEI::FreeDict::Entry;

use strict;

use TEI::FreeDict::Form;

use TEI::FreeDict::Helpers;

sub new
{
  my $class = shift;
  my $title = shift;
  
  my $self;

  if (ref $title eq 'XML::LibXML::Element')
  {
    $self = _load_from_xml($title)
    
  } else
  {
    $self  = {_senses=>[]};
    $self->{_form} = new TEI::FreeDict::Form($title);
  }

  bless($self,$class);
  return $self;
}

sub _load_from_xml
{
  my $node = shift;

  my @children = $node->nonBlankChildNodes();

#  my $form = shift @children;
  my $found = {};

  foreach my $node (@children)
  {
    my $node_name = $node->nodeName;
    if ($node_name eq "form")
    {
       die "Dublicate node 'form' at ".$node->nodePath." at line ".$node->line_number . ". It is not forbidden but not recommended for freedict" if $found->{form};
        
       $found->{form} = 1;
    }
    elsif ($node_name eq "sense")
    {

    } elsif ($node_name eq "gramGrp")
    {

    } else
    {
      TEI::FreeDict::Helpers::_check_node_name_expected_or_die($node, 'form'); # It is not form for sure, and it gives us proper croak message
      print "$node_name\n";
    }
  }

  return {};
}

sub add_sense
{
  my $self = shift;
  my $sense = shift;

  push @{$self->{_senses}}, $sense;
}

sub form
{
  my $self = shift;
  return $self->{_form};
}

sub asString
{
  my $self = shift;

  my $str = "";
  $str .= $self->{_form}->asString;
  foreach (@{$self->{_senses}})
  {
    $str.= $_->asString;
  }
  $str= "      <entry>\n$str      </entry>\n";
  return $str;
}

sub senses 
{
  my $self = shift;
  return  @{$self->{_senses}}
}


sub asPlainTextEntry
{
  my $self = shift;
  my $opt = shift;

  my $str = '';

  $str= $self->form->title unless $opt->{skip_headword};

  $str .= " [".$self->form->pronunciation."]" if $self->form->pronunciation && ! $opt->{skip_pronunciation};
  $str .= "\n" if $str; # Add linefeed only if we have added something before it.

  foreach my $sense (@{$self->{_senses}})
  {
    my $is_first;

    if ( $sense->{_definitions} && @{$sense->{_definitions}})
    {
      $is_first = 1;
      foreach (@{$sense->{_definitions}})
      {
        $str .= ", " unless $is_first;
        $str .= "\t" if $is_first;
        $str .= $_->value;
        $is_first = 0;
      }
      $str.="\n";
      $str.="\n" if $sense->{_translations} && @{$sense->{_translations}};
    }
    if ($sense->{_translations} && @{$sense->{_translations}})
    {
      $is_first = 1;
      foreach (@{$sense->{_translations}})
      {
        $str .= ", " unless $is_first;
        $str .= "\t" if $is_first;
        $str .= $_->value;
        $is_first = 0;
      }
      $str.="\n";
    }
  }

 return $str;
}

1;