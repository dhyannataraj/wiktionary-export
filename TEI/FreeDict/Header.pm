package  TEI::FreeDict::Header;

use strict;
use utf8;

use TEI::FreeDict::Misc  qw(string_XML_encode);
use TEI::FreeDict::Header::Person;

sub new
{
  my $class = shift;
  my %opt = @_;
  
  my $self = {_resp_person=>[]};
  foreach (keys %opt)
  {
    $self->{"_$_"} = $opt{$_};
  }
  
  bless($self,$class);
  return $self;
}

sub title
{
  my $self = shift;
  my $value = shift;

  $self->{_title} = $value if defined $value;
  return $self->{_title};
}

sub edition
{
  my $self = shift;
  my $value = shift;

  $self->{_edition} = $value if defined $value;
  return $self->{_edition};
}

sub extent
{
  my $self = shift;
  my $value = shift;

  $self->{_extent} = $value if defined $value;
  return $self->{_extent};
}

sub publisher
{
  my $self = shift;
  my $value = shift;

  $self->{_publisher} = $value if defined $value;
  return $self->{_publisher};
}

sub availability
{
  my $self = shift;
  my $value = shift;

  $self->{_availability} = $value if defined $value;
  return $self->{_availability};
}

sub pub_date
{
  my $self = shift;
  my $value = shift;

  $self->{_pub_date} = $value if defined $value;
  return $self->{_pub_date};
}

sub pub_url
{
  my $self = shift;
  my $value = shift;

  $self->{_pub_url} = $value if defined $value;
  return $self->{_pub_url};
}

sub series_title
{
  my $self = shift;
  my $value = shift;

  $self->{_series_title} = $value if defined $value;
  return $self->{_series_title};
}

sub source_desc
{
  my $self = shift;
  my $value = shift;

  $self->{_source_desc} = $value if defined $value;
  return $self->{_source_desc};
}


sub project_desc
{
  my $self = shift;
  my $value = shift;

  $self->{_project_desc} = $value if defined $value;
  return $self->{_project_desc};
}

sub add_resp_person
{
  my $self = shift;
  my @params = @_;
  my $person;
  if ( ref($params[0]) eq 'TEI::FreeDict::Header::Person' )
  {
    $person = $params[0];
  } else
  {
   $person = new TEI::FreeDict::Header::Person(@params);
  }
  push @{$self->{_resp_person}}, $person;
}

sub asString
{
  my $self = shift;
  my $str = '  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>'.string_XML_encode($self->title)."</title>\n";
  foreach (@{$self->{_resp_person}})
  {
    $str .= $_->asString."\n";
  }
  $str.="      </titleStmt>\n";
  
  
  $str .='      <editionStmt>
	<edition>'.string_XML_encode($self->edition)."</edition>
      </editionStmt>\n";
      
  $str .='      <extent>'.string_XML_encode($self->extent)."</extent>\n";
  $str .="    <publicationStmt>\n";
  $str .='	<publisher>'.string_XML_encode($self->publisher)."</publisher>\n" if $self->publisher;
  
  if ($self->availability )
  {
    $str .='	<availability>
	  <p>'.string_XML_encode($self->availability)."</p>
	</availability>\n";
  }
  $str .='	<date>'.string_XML_encode($self->pub_date)."</date>\n" if $self->pub_date;
  $str .='	<pubPlace>'.string_XML_encode($self->pub_url)."</pubPlace>\n" if $self->pub_url;

  $str .="      </publicationStmt>\n";
  if ($self->series_title)
  {

    $str .='      <seriesStmt>
	<title>'.string_XML_encode($self->series_title)."</title>
      </seriesStmt>\n";
  }
     # ############ FIXME: Nones are omitted
     #<notesStmt>
     #  <note type="status">new dictionary project</note>
     #  <note>Thanks to ...</note>
     #</notesStmt>
     
  if ($self->source_desc)
  {
    $str .= '     <sourceDesc>
       <p>'.string_XML_encode($self->source_desc)."</p>
      </sourceDesc>\n";
  }
$str.= "    </fileDesc>\n";
  if ($self->project_desc)
  {
    $str.='    <encodingDesc>
        <projectDesc>
          <p>'. string_XML_encode($self->project_desc)."          </p>
      </projectDesc>
    </encodingDesc>\n";
  }
# ############ FIXME revision is omitted for now
#   <revisionDesc>
#       <change>
#	<date>200x-mm-dd</date>
#	<respStmt><name>Someone</name></respStmt>
#	<item>Start</item>
#      </change>
#      <change>
#	<date>2005-04-02</date>
#	<respStmt><name>Peter Gossner / Michael Bunk</name></respStmt>
#	<item>FreeDict Dictionary TEI XML Template V0.1</item>
#      </change>
#    </revisionDesc>

  $str.="  </teiHeader>\n";
  return $str;
}

1;

