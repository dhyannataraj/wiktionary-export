package  TEI::FreeDict::Helpers;

use strict;
use utf8;
use Carp;
use XML::LibXML;


sub _check_node_name_expected_or_die
{
  my $node = shift;
  my $expected_name = shift;

  if ($node->nodeType != XML_ELEMENT_NODE)
  {
    croak "Unexpected node-type ".$node->nodeName." at ".$node->nodePath." at line ".$node->line_number;
  }
  if ($node->nodeName ne $expected_name)
  {
    croak "Unexpected node ".$node->nodeName." at ".$node->nodePath." at line ".$node->line_number;
  }
}
sub _check_node_does_not_exist_or_die
{
  my $node = shift;
  if ($node)
  {
    croak "Unexpected node ".$node->nodeName." at ".$node->nodePath." at line ".$node->line_number;
  }
}

1;
