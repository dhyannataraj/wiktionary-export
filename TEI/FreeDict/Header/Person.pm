package TEI::FreeDict::Header::Person;

use strict;
use utf8;

use TEI::FreeDict::Misc;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {};
  $self->{_name} =  $opt{name}  if $opt{name};
  $self->{_email} = $opt{email} if $opt{email};
  $self->{_role} =  $opt{role}  if $opt{role};
  
  bless($self,$class);
  return $self;
}

sub name
{
  my $self = shift;
  my $value = shift;

  $self->{_name} = $value if defined $value;
  return $self->{_name};
}

sub email
{
  my $self = shift;
  my $value = shift;

  $self->{_email} = $value if defined $value;
  return $self->{_email};
}

sub role
{
  my $self = shift;
  my $value = shift;

  $self->{_role} = $value if defined $value;
  return $self->{_role};
}

sub asString
{
  my $self = shift;
  my $str = "	<respStmt>\n";
  $str.= "	  <resp>".string_XML_encode($self->role)."</resp>\n" if $self->role;
  $str.= "	  <name>".string_XML_encode($self->name);
  $str.= " &lt;".string_XML_encode($self->email)."&gt;";
  $str.= "</name>\n";
  $str.= "	</respStmt>";
  return $str;
}
1;

