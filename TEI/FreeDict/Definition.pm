package TEI::FreeDict::Definition;

use strict;
use utf8;

use TEI::FreeDict::Misc;

sub new
{
  my $class = shift;
  my $value = shift;
  my $self={_value =>$value};
 
  
  bless($self,$class);
  return $self;
}

sub value
{
  my $self = shift;
  return $self->{_value};
}

sub asString
{
  my $self = shift;
  my $str = TEI::FreeDict::Misc::string_XML_encode($self->{_value});
  $str = "          <def>$str</def>\n";
  return $str;
}
1;
