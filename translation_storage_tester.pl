#!/usr/bin/perl

use strict;
use utf8;

use FindBin;
use lib $FindBin::Bin."/.";

use Wiktionary::Page;

use Libredict::TranslationStorage;


binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';


my $pages = {
"беспардонный" => "t/ru.wiktionary.pages/беспардонный.wiki",
"замок" => "t/ru.wiktionary.pages/замок.wiki",
};

my $storage = Libredict::TranslationStorage->new(source_lang => 'ru', target_langs => ['en','fr','de'], logger => \&my_logger);

foreach my $name (keys %$pages)
{
  my $page = new Wiktionary::Page(lang=>'ru');
  $page->load_from_file($pages->{$name}, $name);

  $storage->add_wikipage($page);

next;

=cut
my $from_lang='ru';
#print"===========$name\n";
      eval {$page->get_section($from_lang)->articles;};
      if ($@)
      {
        my $error_msg = $@;
#        write_log($word,$error_msg);
      } else
      {
        my $sec=$page->get_section($from_lang);
        foreach my $art ($sec->articles())
        {
          eval {$art->all_translations};
          if ($@)
          {
            my $error_msg = $@;
#            write_log($word,$error_msg);
          } else
          {
            my $all_trans = $art->all_translations;
            foreach my $to_lang (keys %{$all_trans})
            {
#              use Data::Dumper;
#              print Dumper $all_trans->{$to_lang};
            }
          }
        }
      }

=cut
}

sub my_logger
{
  my $message = join " ", @_;
  print $message
}
