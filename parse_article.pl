#!/usr/bin/perl

use strict;
 use MediaWiki::API;


use utf8;
use Devel::Peek;
use Dict::Word;


open F_TEI, '>:encoding(UTF-8)',"res_tei";
open F_DIR, '>:encoding(UTF-8)',"res_direct";
open F_NO_TRANS, '>:encoding(UTF-8)',"res_no_trans";
open F_COMPLEX, '>:encoding(UTF-8)',"res_too_complex";
open F_COMPLEX_NO_TRANS, '>:encoding(UTF-8)',"res_too_complex_no_trans";
open F_WRONG_FORMAT, '>:encoding(UTF-8)',"res_wrong_format";

print F_TEI tei_header();


my $mw = MediaWiki::API->new();
$mw->{config}->{api_url} = 'http://ru.wiktionary.org/w/api.php';

#$mw->{config}->{use_http_get}=1;
$mw->{config}->{skip_encoding}=1;

my $count=0;
open F, "ru.wictionary_frech_words.txt";
while ((my $word = <F>) && ($count<100 ))
{
  chomp $word;
  Encode::_utf8_on($word);
#  $word="RER" if $count==0;
  
  if ( ! ($word=~/^Категория:/))
  {
    print "$word\n";
    $count++;


    my $page = $mw->get_page( { title => $word } );
    my $text= $page->{'*'};
    my $article=get_fr_article($text);
    my $trans=get_translation($word,$article);
    
    if (! ref ($trans))
    {
      if ($trans eq "Wrong format")
      {
        print F_WRONG_FORMAT "$word \n";
        print " F_WRONG_FORMAT: $word \n";
      } else 
      {
      if( $trans ne "" && $trans==0 )
      {
        print F_COMPLEX "$word\n";
        print "F_COMPLEX  $word\n";
      } else
      {
        if (defined($trans) && $trans eq "" )
        {
          print F_COMPLEX_NO_TRANS "$word\n";
          print "F_COMPLEX_NO_TRANS: $word\n";
          
        } else
        {
          print  F_NO_TRANS "$word\n";
          print  "F_NO_TRANS: $word\n";
        }
        }
      }
    }

    if (ref $trans eq 'Dict::Word')
    {
      my $s="";
      my $i=1;
      print F_TEI word_as_tei_enrty($trans);
      
      foreach (@{$trans->senses})
      {
        $s.="$i. $_ ";
        $i++;
      }
      print F_DIR "$word $s\n";
      print " DIRECT: $word $s\n";
    }
  }
}

print F_TEI tei_footer();

close F_TEI;
close F_DIR;
close F_NO_TRANS;
close F_COMPLEX;
close F_COMPLEX_NO_TRANS;
close F_WRONG_FORMAT;

sub get_fr_article
{
  my $text=shift;
  $text=~s/(^.*\=\ \{\{\-fr\-\}\}\ \=)//s;
  $text=~s/(\=\ \{\{\-[a-z][a-z]\-\}\}\ \=.*$)//s;
  return $text
}

sub get_translation
{
  my $word = shift;
  my $article = shift;
  
  if (! check_article_format($article))
  {
    return ("Wrong format");
  }
  
  $article=~/={3,4}+\ Значение\ ={3,4}+(.*?)={3,4}+/s;
  my $trans=$1;
  if ($trans=~/\{\{Нужен\ перевод\}\}/s)
  {
    $trans=~s/\{\{Нужен\ перевод\}\}//sg; #clear need translation flag
    $trans=~s/\[\[\]\]//sg;               #clear empty links
    $trans=~s/\s//sg;                     #clear all white spases
    return undef if $trans eq ""; # translation is really empty
    return ""; #too complex
  }

  
  print "====================\n$trans \n==================\n";
  #trying to parse entry as a wiki list
  if (my $list = parse_numered_list($trans))
  {
    my $w=Dict::Word->new($word);
    foreach (@$list)
    {
      $w->add_sense(string_dewikification($_));
    }
    return $w;
  }
  if (my $single_line = parse_single_lined_entry($trans))
  {
    my $w=Dict::Word->new($word);
    $w->add_sense(string_dewikification($single_line));
    return $w;
  }

  return 0; # too complex for now
}


sub parse_numered_list
{
  my $str=shift;
  my @l=split /\n/,$str;

  #clear all whitespases before and after the numbered list
  while ($#l>=0 && $l[0]=~/^[\#\s]?\s*$/) { shift @l;}
  while ($#l>=0 && $l[-1]=~/^[\#\s]?\s*$/) { pop @l;}
  for (my $i=0;$i<=$#l;$i++)
  {
    if ($l[$i]=~/^\#\s*(.*)$/)
    {
      $l[$i]=$1;      #if there is one more item of the list remove # at the begining
    } else
    {
      return undef   # if some string spoils the list we do not parse it
    }
  }
  return undef if $#l<0;
  return \@l;
}

sub parse_single_lined_entry
{
  my $str=shift;
  my @l=split /\n/,$str;

  #clear all whitespases before and after the numbered list
  while ($#l>=0 && $l[0]=~/^[\#\s]?\s*$/) { shift @l;}
  while ($#l>=0 && $l[-1]=~/^[\#\s]?\s*$/) { pop @l;}
  for (my $i=0;$i<=$#l;$i++)
  {
    if (! $l[$i]=~/\S/)
    {
      return undef   # if there is empty line at the middle of the entry, it is now wiki one line entry...
    }
  }
  return undef if $#l<0;
  return join " ",@l;
}


sub check_article_format
{
   my $text = shift;
   if ($text=~/^={3,4}?\ Значение\ ={3,4}?$/m)
   { 
    return 1;
   }
   return undef;
}

sub string_dewikification
{
  my $str = shift;
  $str=~s/\{\{пример\|\}\}//g; # remove empty examples
  $str=~s/\[\[[^\]]*?\|([^\]]*?)\]\]/$1/g; # remove wiki links with double-names
  $str=~s/\[\[([^\]]*?)\]\]/$1/g; # remove other wiki-links
  
  return $str;
}


sub string_deHTML
{
  my $str = shift;
  $str=~s/\</\&lt\;/g;
  $str=~s/\>/\&gt\;/g;
  return $str;
}



sub word_as


_tei_enrty
{
  my $word = shift;
  my $orth =string_deHTML($word->orthography);
  
  my $s = qq(<entry>
  <form>
	  <orth>$orth</orth>
	  <pron></pron>
  </form>
<!--  <gramGrp>
    <pos>n</pos><gen>m</gen>
  </gramGrp> -->
  );
  foreach (@{$word->senses})
  {
    my $sense=string_deHTML($_);
    $s.=qq(
    <sense>
     <trans><tr>$sense</tr></trans>
	</sense> );
  }
  $s.=qq(
</entry>) ;

return $s;
}




sub tei_header
{
  return qq(<?xml version="1.0" encoding="UTF-8"?>
<!--
<!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "http://www.tei-c.org/Guidelines/DTD/tei2.dtd" [
-->
<!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "file://home/nataraj/tei/dtd/tei2.dtd" [
<!ENTITY % TEI.dictionaries "INCLUDE">
<!ENTITY % TEI.XML "INCLUDE">
<!ENTITY % TEI.linking "INCLUDE">
<!ATTLIST xptr url CDATA #IMPLIED>
<!ATTLIST xref url CDATA #IMPLIED>
]>
<TEI.2>
  <teiHeader>
    <fileDesc>
      <titleStmt>
	<title>Language1-Language2 FreeDict Dictionary</title>
	<respStmt>
	  <resp>Author</resp>
	  <name>Someone Somewho &lt;her\@email.address&gt;</name>
	</respStmt>
	<respStmt>
	  <resp>Maintainer</resp>
	  <name>Someone Else &lt;or\@the-same.example.com&gt;</name>
	</respStmt>
      </titleStmt>
      <editionStmt>
	<edition>0.1</edition>
      </editionStmt>
      <extent>less than 1000 headwords</extent>
      <publicationStmt>
	<publisher>FreeDict</publisher>
	<availability>
	  <p>GNU GENERAL PUBLIC LICENSE</p>
	</availability>
	<date>200X</date>
	<pubPlace>http://freedict.org/</pubPlace>
      </publicationStmt>
      <seriesStmt>
	<title>free dictionaries</title>
      </seriesStmt>
     <notesStmt>
       <note type="status">new dictionary project</note>
       <note>Thanks to ...</note>
     </notesStmt>
     <sourceDesc>
       <p>Home: <xptr url="http://www.somewhere.in.cspace/"/></p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
        <projectDesc>
          <p>This dictionary comes to you through nice people making it
          available for free and for good. It might be part of the FreeDict project,
          http://www.freedict.org / http://freedict.de. This
          project aims to make available many translating dictionaries
          for free. Your contributions are welcome!</p>
      </projectDesc>
    </encodingDesc>
   <revisionDesc>
       <change>
	<date>200x-mm-dd</date>
	<respStmt><name>Someone</name></respStmt>
	<item>Start</item>
      </change>
      <change>
	<date>2005-04-02</date>
	<respStmt><name>Peter Gossner / Michael Bunk</name></respStmt>
	<item>FreeDict Dictionary TEI XML Template V0.1</item>
      </change>
    </revisionDesc>
  </teiHeader>

  <text>
    <body>
  
  
  );
}

sub tei_footer
{
  return qq(</body>
  </text>
</TEI.2>
  );
}