#!/usr/bin/perl

use strict;
use utf8;
use Encode;
binmode STDOUT, ':utf8';


use MediaWiki::API;

# This script will temproary solve the problem with leaking memory in IO::Socket::SSL
# Since all memory will be free when srcipt will exit
# Check status of
# https://rt.cpan.org/Ticket/Display.html?id=123520
# https://rt.cpan.org/Ticket/Display.html?id=120643
# https://rt.cpan.org/Ticket/Display.html?id=121192
# before removing it and fall back to $server->get_page($word);


my $word = $ARGV[0];
$word = Encode::decode_utf8($word);

die "Usage: $0 [article_name_to_fetch]" unless $word;

my $mw = MediaWiki::API->new();
$mw->{config}->{api_url} = 'https://ru.wiktionary.org/w/api.php';
my $page = $mw->get_page( { title => $word }, {skip_encoding => 1} );

die "Error loading page '$word'" unless $page;
die "Page '$word' is missing" if exists $page->{missing};

print $page->{'*'};

