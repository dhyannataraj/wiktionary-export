#!/usr/bin/perl

use strict;
use utf8;
use MediaWiki::API;
binmode STDOUT, ':utf8';

my $mw=MediaWiki::API->new();
$mw->{config}->{api_url} = 'http://ru.wiktionary.org/w/api.php';

my $articles = $mw->list ( {
	action => 'query',
	list => 'categorymembers',
	cmtitle =>  'Категория:Викисловарь:Шаблоны:Языки',
#	cmnamespace => 0, # Common articles, not subcatigories
	cmlimit => 'max' } , {skip_encoding => 1} 
    ) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
my @res =();
foreach (@$articles)
{
  my $title = $_->{title};
  if ($title=~/^Шаблон:-(.*)-$/)
  {
    my $lang_code=$1;
    my $page = $mw->get_page( { title => $title }, {skip_encoding => 1} ) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
    my $page_body = $page->{'*'};
    my $lang_name = undef;
    if ($page_body =~ /lang\=(.*?)\|/s)
    {
      $lang_name=$1;
    } 
    print "'$lang_code'\t=>'$lang_name',\n" if $lang_name;
    
  }
}

