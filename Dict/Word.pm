package Dict::Word;

use strict;

our $VERSION  = "0.01";

sub new 
{
  my $class = shift;
  my $orthography = shift;
  
  my $self={_orthography=>$orthography};
  bless ($self, $class);
  return $self;
}

sub senses
{
  my $self=shift;
  return $self->{_senses};
}

sub add_sense
{
  my $self = shift;
  my $sense = shift;
  $self->{_senses}=[] unless  $self->{_senses};
  push @{$self->{_senses}},$sense;
}

sub orthography
{
  my $self = shift;
  return $self->{_orthography};
}


1;