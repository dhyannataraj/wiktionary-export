package Wiktionary::Article;

use strict;
use warnings;
use Carp qw(cluck);
use Scalar::Util qw( weaken );

sub new
{
  my $class = shift;
  my %opt = @_;
  
  my $section = $opt{section};
  my $self = undef;
  
  if ($section->page_lang eq 'ru')
  {
    use Wiktionary::Ru::Article;
    $self = new Wiktionary::Ru::Article(%opt);
  }
  if ($section->page_lang eq 'en')
  {
    use Wiktionary::En::Article;
    $self = new Wiktionary::En::Article(%opt);
  }
  $self->_init(%opt) if $self;
  return $self;
}

sub _init
{
  my $self = shift;
  my %opt = @_;
  
  $self->{_section} = $opt{section};
  weaken($self->{_section});
  $self->{_server} = $self->{_section}->server;
  weaken($self->{_server});
  $self->{_title} = $self->{_section}->page->title;
  $self->{_header} = $opt{header} if $opt{header};
  $self->{_lang} = $self->{_section}->page_lang;
  if ( $opt{src} )
  {
    $self->_parse_src($opt{src});
  }
}

sub title
{
  my $self = shift;
  return $self->{_title};
}

sub subtitle
{
  my $self = shift;
  return $self->{_subtitle};
}

sub server
{
  my $self = shift;
  return $self->{_server};
}

sub section
{
  my $self = shift;
  return $self->{_section};
}

sub page
{
  my $self = shift;
  return $self->section->page;
}

sub lang
{
    cluck "Wiktionary::Article::Lang is depricated!";
  my $self = shift;
  return $self->{_lang};
}

sub page_lang
{
  my $self = shift;
  return $self->{_lang};
}

sub word_lang
{
  my $self = shift;
  return $self->section->word_lang;
}

sub traslation_langs
{
  my $self = shift;
  return keys %{$self->{_translation}};
}

sub all_translations
{
  my $self = shift;
  if (! %{$self->{_translation}})
  {
    $self->_parse_src($self->{_body});
  }
  return $self->{_translation};
}

#sub DESTROY 
#{
#  print "destroing article\n";
#}
1;