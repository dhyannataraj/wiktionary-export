package Wiktionary::En::Page;
use base 'Wiktionary::Page';

use Wiktionary::Section;


use strict;
use warnings;
use utf8;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {};
  bless $self,$class;
  return $self;
}

sub _split_on_sections
{
  my $self = shift;
  my $src = $self->{_body};
  
  $src=~s/^(.*?)(\=\s*\{\{\-(?:[a-z]{2,3}?|INT)\-(?:\|.*?|)\}\}\s*\=)/$2/s;
  $self->{_prefix}=$1;
  if ($2 eq "") {die "Page ".$self->{_title}." does not have proper language header " };
  
  if (! ($self->{_prefix}=~/^\s*$/s)) { warn "Page ".$self->{_title}." has non emply prefix, prehaps wrong formatting";}
  
  my @l=();
  while ($src=~s/(\=\s*\{\{\-(?:[a-z]{2,3}?|INT)\-(?:\|.*?|)\}\}\s*\=)(.*?)(\=\s*\{\{\-(?:[a-z]{2,3}?|INT)\-(?:\|.*?|)\}\}\s*\=)/$3/s)
  {
    push @l,$1.$2;
  }
  push @l,$src;
  
  $self->{_sections}={};
  foreach my $as_text (@l)
  {
    $as_text=~/(\=\s*\{\{\-([a-z]{2,3}?|INT)\-(?:\|.*?|)\}\}\s*\=)(.*)$/s;
    my $header = $1;
    my $lang = $2;
    my $body_text=$3;

    if ($self->{_sections}->{$lang}) {die "Page ".$self->{_title}." has more then one entry for $lang language title";}
    $self->{_sections}->{$lang}= new Wiktionary::Section(page => $self, header => $header, body => $body_text, section_lang => $lang);
  }
  return 1;
}

#sub sections_langs
#{
#  my $self = shift;
#  if (! %{$self->{_sections}})
#  {
#    $self->_split_on_sections;
#  }
#  return keys %{$self->{_sections}};
#}

#sub get_section
#{
#  my $self = shift;
#  my $lang = shift;
#  if (! %{$self->{_sections}})
#  {
#    $self->_split_on_sections;
#  }
#  return $self->{_sections}->{$lang};
#}



1;
