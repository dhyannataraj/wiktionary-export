package Wiktionary::En::Server;

use base 'Wiktionary::Server';

use strict;
use utf8;

my %language_ctegories=
(
  'ru' => "Category:Russian_language",
  'en' => "Category:English_language",
  'fr' => "Category:French_language",
  'de' => "Category:German_language",
  'es' => "Category:Spanish_language",
  'it' => "Category:Italian_language",
  'mi' => "Category:Maori_language",
  
);

sub new
{
  my $class = shift;
  my $self = {
    _lang    =>'en',
    _api_url =>'http://en.wiktionary.org/w/api.php',
    _language_catigories => \%language_ctegories,
    };
  bless $self,$class;
  return $self;
}
1;
