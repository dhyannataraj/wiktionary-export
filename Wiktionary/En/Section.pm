package Wiktionary::En::Section;
use base 'Wiktionary::Section';

use Wiktionary::Article;


use strict;
use utf8;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {
     _body => $opt{body},
     _header => $opt{header},
     _articles => [],
     _section_lang => $opt{section_lang},
  };
  bless $self,$class;
  return $self;
}


#sub _parse
#{
#  my $self = shift;
#  my $header = shift;
#  my $body = shift;
#  
#  my $title = $self->title;
#
#  $self->{_header} = $header;
#  $self->{_body} = $body;
#
#  $header=~/\=\s*\{\{\-([a-z]{2,3}?|INT)\-(?:\|.*?|)\}\}\s*\=$/s;
#  $self->{_lang} = $1;
#  
#  print "Setting section lang:". $self->{_lang}."\n";
#
#  my @articles = ();

#  my @a_bodies = $self->_split_section_on_subarticles($body);
#  foreach my $a_body (@a_bodies)
#  {
#    $a_body=~s/^(\=\=\.*\=\=)//s; 
#    my $article = new Wiktionary::Article(section=>$self, body=>$a_body);
#    push @articles, $article;
#  }
#  $self->{_articles} = \@articles;
#}

#sub _split_section_on_subarticles_old
#{
#  my $self = shift;
#  my $body = shift;
#  my $title = $self->title;
#  if ($body=~/\=\=\s*${title}\s+[IVX]+\s*\=\=/s )
#  {
#    my @l = split /\=\=\s*${title}\s+[IVX]+\s*\=\=/s , $body;
#    if (! (@l[0]=~/^\s*$/s))
#    {
#      warn "Section '".$self->{_section_lang}."' on page '".$self->{_title}."' has non emply prefix, perhaps wrong formatting";
#    }
#    shift @l;
#    return @l;
#  }
#  if ($body=~/\=+\s*([сС]уществительное|[пП]рилагательное|[гГ]лагол|[нН]аречие)\s*\=+/s )
#  {
#     my @l = split /\=+\s*(?:[сС]уществительное|[пП]рилагательное|[гГ]лагол|[нН]аречие)\s*\=+/s , $body;
#    if (! (@l[0]=~/^\s*$/s))
#    {
#      warn "Section '".$self->{_section_lang}."' on page '".$self->{_title}."' has non emply prefix, perhaps wrong formatting";
#    }
#    shift @l;
#    return @l;
#  }
#  if ($body=~/\{\{заголовок\|[a-z]{2,3}\|add\=[IVX]+\}\}/s )
#  {
#    my @l = split /\{\{заголовок\|[a-z]{2,3}\|add\=[IVX]+\}\}/s , $body;
#    if (! (@l[0]=~/^\s*$/s))
#    {
#      warn "Section '".$self->{_section_lang}."' on page '".$self->{_title}."' has non emply prefix, prehaps wrong formatting";
#    }
#    shift @l;
#    return @l;
#  }
#  
#  return $body;
#}

sub _split_section_on_subarticles
{
  my $self = shift;
  my $body = shift;
  my $title = $self->title;
  
  my $subtitle ="";
  my $buff = "";
  my @parts = ();
  foreach my $line (split /\n/, $body)
  {
    my $new_subtitle ="";
    if ($line=~/^\=\=([^\=].*?)\s*\=\=/)
    {
      $new_subtitle = $1;
      $new_subtitle=~s/^\s*//; # remove front spases
      if ( $line!~/${title}/ && $line!~/([сС]уществительное|[пП]рилагательное|[гГ]лагол|[нН]аречие)/)
      {
        warn "Unknown section subtitle '$new_subtitle' on page '".$self->title."', section '".$self->{_section_lang}."'";
        $new_subtitle ="";
      }
    } elsif ( ($line=~/(\{\{заголовок\|[a-z]{2,3}\|add\=.+?\}\})/s) ||
              ($line=~/(\{\{заголовок\|add\=.+?\}\})/s)
            )
    {
      $new_subtitle = $1;
    }
    
    if ($new_subtitle)
    {
      if (! $subtitle)
      {
        if ($buff !~/\s*/)
        {
          warn "Section '".$self->{_section_lang}."' on page '".$self->title."' has non emply prefix, perhaps wrong formatting";
        } 
      } else
      {
        my $part={title => $subtitle, body => $buff};
        push @parts, $part;
      }
      $buff="";
      $subtitle = $new_subtitle;
    } else
    {
      $buff.=$line."\n";
    }
  }
  my $part={title => $subtitle, body => $buff};
  push @parts, $part;
  return @parts;
  
}

sub _split_on_articles
{
  my $self = shift;
  $self->{_articles} = [];
  foreach my $article_part ($self->_split_section_on_subarticles($self->{_body}))
  {
     my $article = new Wiktionary::Article(section=>$self, body=>$article_part->{body}, subtitle=>$article_part->{title});
     push @{$self->{_articles}}, $article;
  }
}

sub articles
{
  my $self = shift;
  if (! @{$self->{_articles}})
  {
    $self->_split_on_articles;
  }
  return @{$self->{_articles}};
}
1;
