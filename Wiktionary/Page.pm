package Wiktionary::Page;

use strict;
use warnings;
use utf8;
use Scalar::Util qw( weaken );


sub new
{
  my $class = shift;
  my $self = undef;
  my %opt = @_;
  my $lang = $opt{lang};
  
  warn "Owerriding lang=$lang with server lang= $opt{server}->lang" if $lang && $opt{server} && $opt{server}->lang && $lang ne $opt{server}->lang;
  $lang = $opt{server}->lang if $opt{server} && $opt{server}->lang;
  
  if ($lang eq 'ru')
  {
    use Wiktionary::Ru::Page;
    $self =  new Wiktionary::Ru::Page(%opt);
  } elsif ($lang eq 'en')
  {
    use Wiktionary::En::Page;
    $self =  new Wiktionary::En::Page(%opt);
  } else
  {
    die "Do not know lang $lang. Aborting";
  }
  $self->_init(%opt) if $self;
  return $self;
}

sub _init
{
  my $self = shift;
  my %opt = @_;
  $self->{_server} = $opt{server};
  weaken($self->{_server});
  $self->{_title}  = $opt{title};
  $self->{_body} = $opt{body};
  $self->{_lang} = $opt{lang};
  $self->{_sections} = {};
}

sub src
{
  my $self = shift;
  return $self->{_body};
}

sub server
{
  my $self = shift;
  return $self->{_server};
}

sub title
{
  my $self = shift;
  return $self->{_title};
}

sub load_from_file
{
  my $self = shift;
  my $file_name = shift;
  my $title = shift;
  
  die "File '$file_name' do not exist" unless -e $file_name;
  die "Please specify page title" unless $title;
  
  $self->{_title} = $title;
  
  open F,'<:utf8', $file_name or die "Error opening file '$file_name'";
  my $str = '';
  while (my $s =<F>)
  {
    $str.=$s;
  }
  $self->{_body} = $str;
  $self->{_sections}={};
  close F;
}

sub sections_langs
{
  my $self = shift;
  if (! %{$self->{_sections}})
  {
    $self->_split_on_sections;
  }
  return keys %{$self->{_sections}};
}

sub get_section
{
  my $self = shift;
  my $lang = shift;
  if (! %{$self->{_sections}})
  {
    $self->_split_on_sections;
  }
  return $self->{_sections}->{$lang};
}

sub lang
{
  my $self = shift;
  return $self->{_server}->lang if $self->{_server};
  return $self->{_lang};
  
}

#sub DESTROY 
#{
#  print "destrioing page\n";
#}

1;

