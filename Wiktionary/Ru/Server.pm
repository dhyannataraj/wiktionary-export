package Wiktionary::Ru::Server;

use base 'Wiktionary::Server';

use strict;
use utf8;

my %language_ctegories =
(
  'ru' => "Категория:Русский_язык",
  'en' => "Категория:Английский_язык",
  'fr' => "Категория:Французский_язык",
  'de' => "Категория:Немецкий_язык",
  'es' => "Категория:Испанский_язык",
  'it' => "Категория:Итальянский_язык",
  'mi' => "Категория:Маори",
  'lv' => "Категория:Латышский_язык",
  'fi' => "Категория:Финский_язык",
  'la' => "Категория:Латинский_язык",
  'ltg' => "Категория:Латгальский_язык",
  'sah' => "Категория:Якутский_язык",
  'chm' => "Категория:Марийский_язык",
  'tt' => "Категория:Татарский_язык",
  'ba' => "Категория:Башкирский_язык",
  'os' => "Категория:Осетинский_язык",
  'myv' => "Категория:Эрзянский_язык",
  'cv' => "Категория:Чувашский_язык",
  'ce' => "Категория:Чеченский_язык",
  'kk' => "Категория:Казахский_язык",
#   'fr' => "Категория:Редкие_выражения/fr",
);

sub new
{
  my $class = shift;
  my $self = {
    _lang    =>'ru',
    _api_url =>'https://ru.wiktionary.org/w/api.php',
    _language_catigories => \%language_ctegories,
    };
  bless $self,$class;
  return $self;
}
1;