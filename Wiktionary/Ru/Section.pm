package Wiktionary::Ru::Section;
use base 'Wiktionary::Section';

use Wiktionary::Article;


use strict;
use utf8;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {
     _body => $opt{body},
     _header => $opt{header},
     _articles => [],
     _section_lang => $opt{section_lang},
  };
  bless $self,$class;
  return $self;
}


sub _split_section_on_subarticles
{
  my $self = shift;
  my $body = shift;
  my $title = $self->title;
  
  my $subtitle ="";
  my $buff = "";
  my @parts = ();
  foreach my $line (split /\n/, $body)
  {
    my $new_subtitle ="";

    if ($line=~/^\=\=([^\=].*?)\s*\=\=/)
    {
      $new_subtitle = $1;
      $new_subtitle=~s/^\s*//; # remove front spases
      my $is_found = 0;
      if ( $line=~/${title}/  || #FIXME this should be outdated
           $line=~/\{\{PAGENAME\}\}/||  #FIXME this should be outdated
           $line=~/^\s*([сС]уществительное|[пП]рилагательное|[гГ]лагол|[нН]аречие|[Сс]оюз|[Мм]еждометие|[Мм]естоимение)/  #FIXME this should be outdated
         )
      {
         warn "Outdated section subtitle '$new_subtitle' on page '".$self->title."', section '".$self->{_section_lang}."'";
         $is_found=1;
      }
      if ($line=~/\{\{заголовок\}\}/)
      {
         my $title_text = $1;
         $is_found = 1;
      }

      if ($line=~/\{\{заголовок\|(.*?)\}\}/ ||
          $line=~/\{\{з\|(.*?)\}\}/)
      {
         my $title_text = $1;
         $is_found= check_zagolovok_template($title_text);
      }
      if (! $is_found)
      {
        warn "Unknown section subtitle '$new_subtitle' on page '".$self->title."', section '".$self->{_section_lang}."'";
#        $new_subtitle ="";
      }
    }

    if ($new_subtitle)
    {
      if (! $subtitle)
      {
        if ($buff !~/\s*/)
        {
          warn "Section '".$self->{_section_lang}."' on page '".$self->title."' has non emply prefix, perhaps wrong formatting";
        } 
      } else
      {
        my $part={title => $subtitle, body => $buff};
        if ( ($buff !~ /\{\{plur.*?\}\}/) &&  # ignoring articles with {{plur}} template, they are not real articles   #FIXME here possible error we treat {{plur_anythin as {{plur
             ($buff !~ /\{\{forms.*?\}\}/) && # same for {{forms}}
             ($buff !~ /\{\{словоформа[\|\s].*?\}\}/)  # same for {{словоформа}}
           )
        {
          push @parts, $part;
        }
      }
      $buff="";
      $subtitle = $new_subtitle;
    } else
    {
      $buff.=$line."\n";
    }
  }
  my $part={title => $subtitle, body => $buff};
  if ( ($buff !~ /\{\{plur.*?\}\}/) &&  # ignoring articles with {{plur}} template, they are not real articles #FIXME here possible error we treat {{plur_anythin as {{plur
       ($buff !~ /\{\{forms.*?\}\}/) &&  # same for {{forms}}
       ($buff !~ /\{\{словоформа[\|\s].*?\}\}/)  # same for {{словоформа}}
     )
  {
    push @parts, $part;
  }
  return @parts;
}

sub _split_on_articles
{
  my $self = shift;
  $self->{_articles} = [];
  foreach my $article_part ($self->_split_section_on_subarticles($self->{_body}))
  {
     my $article = new Wiktionary::Article(section=>$self, body=>$article_part->{body}, subtitle=>$article_part->{title});
     push @{$self->{_articles}}, $article;
  }
}

sub check_zagolovok_template
{
  my $s = shift;
  my $s_copy = $s; # For error message

  my @parts = split(/\|/, $s);
  undef $s;
foreach my $ss (@parts)
  {
    next if $ss=~/\=/; # skip all name=value cases
    if ($s)
    {
       # if have it second tume, it is not right;
       goto err;
    }
    $s = $ss;
  }

  return 1 if !defined $s; # there was only name=value prams

  my $part_of_speech = undef;
  if ( $s =~ /^\s*[IVX]+\s*$/)
  {
    return 1;
  }

  if ( $s =~ /^\s*\(([A-Яа-я]*)(\s+[IVX]+|)\)/ )
  {
    $part_of_speech = $1;
    return 1 if ($part_of_speech eq '' && $2 ne '');
  } elsif ( $s =~ /^[IVX]+\s*\(([A-Яа-я]*)\)/ )
  {
    $part_of_speech = $1;
  }
  if ($part_of_speech =~ /[сС]уществительное|[пП]рилагательное|[гГ]лагол|[нН]аречие|[Сс]оюз|[Мм]еждометие|[Мм]естоимение|[Пп]редлог|[Чч]ислительное/)
  {
     return 1;
  }

err:
  warn "Unexpected content in заголовок template $s_copy: $s";
  return 0;
}

1;
