package Wiktionary::Ru::Article;
use base 'Wiktionary::Article';

use strict;
use utf8;


use TEI::FreeDict::Entry;
use TEI::FreeDict::Sense;
use TEI::FreeDict::Definition;
use TEI::FreeDict::Translation;

use MediaWiki::Parser;


sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = { 
      _body => $opt{body},
      _translation => {},
      _subtitle => $opt{subtitle},
      _section =>$opt{section},
  };
  bless $self,$class;
  return $self;
}

sub _parse_src
{
  my $self = shift;
  my $body = shift;
  $self->{_body} = $body;
  
  my $parser = new MediaWiki::Parser;
  # $parser->debug(1) if $self->server->debug;
  
  my $body_DOM =  $parser->parse($body);
  if ($self->page_lang eq $self->word_lang)
  {
    $self->_parse_src_native($body_DOM);
  }


  {
      my @translations = $body_DOM->subsections_by_title('Значение');
    if ($#translations > 0)
    {
      die "More than one 'Значение' title in article on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
    }
    if ($#translations < 0)
    {
      die "No 'Значение' title in article on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
    }
  
    $self->{_tr_body} = $translations[0]->body_as_string;
    $self->{_tr_body_cleaned} = $self->_tr_cleanup($self->{_tr_body}, $self->title, $self->word_lang, $self->subtitle);
  
    $self->{_translations} = {ru => undef} unless $self->{_translations};

    my $tei_entry = new TEI::FreeDict::Entry($self->title);
  
    if ($self->{_tr_body_cleaned} =~ /^\#?\s*$/)
    {
      die "Section 'Значение' is empty on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
    }
    my $single_tr = $self->_parse_tr_single_line($self->{_tr_body_cleaned}, $self->word_lang, 'ru' );
    if ( ref $single_tr )
    {
      $tei_entry->add_sense($single_tr);
    } else
    {
      foreach my $sense ( $self->_parse_tr_numbered_list($self->{_tr_body_cleaned}, $self->word_lang, 'ru' ))
      {
        $tei_entry->add_sense($sense)
      }
    }
    $self->{_translation}->{ru} = $tei_entry;
  }
  
#  print "Starting pronons_parsing\n";
  my @pronons = $body_DOM->subsections_by_title('Произношение');
  if ($#pronons > 0)
  {
    die "More than one 'Произношение' title in article on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
  }
  if ($#pronons == 0)
  {
    my $pronon_paragraph = $pronons[0]->body_as_string;
    $pronon_paragraph=~s/\{\{медиа\|.*?\}\}//s; #remove media template
    if ( ($pronon_paragraph =~ /^\s*\{\{transcriptions?\|{1,2}?\}\}\s*/s ) || ($pronon_paragraph  =~ /^\s*$/s) )
    {
      # empty translation, do nothing
    } else
    {
      if ($pronon_paragraph =~ /^\s*\{\{transcriptions?\|(.*?)\}\}\s*/s)
      {
        my $pronon_body = $1;
#        print "pronon_body = $pronon_body\n";
        $pronon_body =~ /^(.*?)\|/s;
        my $pronon=$1;
        foreach my $to_lang (keys %{$self->{_translation}})
	{
          $self->{_translation}->{$to_lang}->form->pronunciation($pronon);
        }
      } else
      {
        warn "Prononsation paragraph is too complex on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
      }
    }
  }
}

sub _parse_src_native
{
  my $self = shift;
  my $body_DOM = shift;

  my @translations = $body_DOM->subsections_by_title('Перевод');
  if ($#translations > 0)
  {
    die "More than one 'Перевод' title in article on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
  }
  if ($#translations < 0)
  {
    die "No 'Перевод' title in article on page '".$self->title."', section '".$self->word_lang."', subsection '".$self->subtitle."'";
  }
  $self->{_tr_body} = $translations[0]->body_as_string;
  my $str = $translations[0]->body_as_string;
  while ($str=~s/(\{\{перев\-блок(?:.*?(:?\{\{.*?\}\})*?)*\}\})//s) # this can't be understand, it is write only regexp ;-)
  {
    my $block = $1;
    $block =~ s/\<\!\-\-.*?\-\-\>//gs; # remove comments
    $block =~ s/^\s*$//g; # removing empty lines

    $block =~ /^\{\{перев\-блок\|([^\|]*)\|[^\|]*(\|.*)\}\}$/s;
    my $explanation = $1;
    my $body = $2;
    while ($block)
    # This regexp splist string by "|" ignoring thouse "|" that are inside [[]] or {{}}
    {
      my $lang;
      my $line;
      if ($block =~ s/\|([a-zA-Z\-\.]*)\=((?:.*?(:?\[\[.*?\]\])*?(:?\{\{.*?\}\})*?)*)\|/\|/s)
      {
        $lang = $1;
        $line = $2;
      } else
      {
        $block =~ /\|([a-zA-Z\-\.]*)\=(.*)\}\}\s*$/s ;
        $lang = $1;
        $line = $2;
        $block = undef;
      }
      chomp($line);
      $line = $self->_native_tr_cleanup($line, $self->title, $lang);
      if ($line !~/^\s*$/)
      {
        my $sense = $self->_parse_tr_single_line($line, 'ru', $lang);
        if (ref($sense))
        {
          if (! $self->{_translation}->{$lang})
          {
            $self->{_translation}->{$lang} = new TEI::FreeDict::Entry($self->title);
          }
          $self->{_translation}->{$lang}->add_sense($sense);
        } else
        {
          warn "Error in translation into '$lang' on page '".$self->title."' section '".$self->lang."': \"$sense\"";
        }
      }
    }
  }
}


sub _tr_cleanup
{
  my $self = shift;
  my $tr = shift;
  my $title = shift;
  my $lang_title = shift;
  my $sect_title = shift;
  
  $tr=~s/\<\!\-\-.*?\-\-\>//gs; # remove comments

  # Clean &#32; after templates. Some pages has it, and it is almost safe. But issue a warning, as it should be fixed.
  if ($tr =~ s/\}\}\&\#32\;/\}\} /gs)
  {
      warn  "Cleaned &#32; seq after template in $title, section $lang_title, subsection $sect_title";
  }

  # Oher &#32; is not cleanedn, but warning is issued.
  if ($tr =~ /\&\#32\;/)
  {
      warn  "Page $title has &#32;  in translation section in $lang_title, subsection $sect_title. This should be fixed";
  }

  # {{прото|скребущий, тот, кто скребёт}} see https://ru.wiktionary.org/wiki/racleur
  $tr=~s/\{\{прото\|.*?}\}//gs; # Ignore it for now. FIXME, better to parse it too

  # {{перех.|fr}} see https://ru.wiktionary.org/wiki/manger
  # {{неперех.|fr}} see https://ru.wiktionary.org/wiki/manger
  $tr=~s/\{\{перех\..*?}\},//gs;  # так как эти пометы могут быть через запятую, то выпарсим и ее тоже...
  $tr=~s/\{\{неперех\..*?}\},//gs;
  $tr=~s/\{\{перех\..*?}\}//gs;
  $tr=~s/\{\{неперех\..*?}\}//gs;

  if ($tr =~/(\[\[Файл:.*?\]\])/)
  {
      warn "Page $title has image inserted as $1 in $title, section $lang_title, subsection $sect_title ";
      $tr =~s/\[\[Файл:.*?\]\]//g; # Remove links to images, as we do not parse them but they can be placed in "Значение" section and spoil everything
  }
  $tr =~s/\{\{илл\|.*?\}\}//g; # this template insert images, Ignore them too
  $tr =~s/\{\{списки семантических связей\}\}//g; # This template has section header inside, but this parser does not know about it, so better to ignore it
  $tr =~s/\{\{списки семантических связей\|.*?\}\}//g;

  $tr =~ s/\{\{(семантика|семантика\|.*?)\}\}//g; # ignore {{семантика|..}} template

  $tr=~s/\[\[\]\]//g; # remove empty wiki-links
  
  $tr=~s/\{\{пример\|?\}\}//g; # remove empty examples
  $tr=~s/\{\{пример\|[a-z]{2,3}\}\}//g; # remove empty examples
  $tr=~s/\{\{пример\|\|.*?\}\}//g; # remove empty examples

  $tr=~s/\{\{Нужен\ перевод\}\}//sg; # remove "translation needed"
  $tr=~s/\{\{Нужен\ перевод\|[a-z]{2,3}\}\}//sg; # remove "translation needed"

  # {{помета.|...}} (Do not confuse with {{помета|...}} ) is a remider for editors that label should be added.
  # We clean it up, as it is not needed in dictionary releases.
  $tr=~s/\{\{помета\.(?:\|[a-z]{2,3}|)}\}//sg; 
  
  $tr=~s/\{\{длина\ слова.*?\}\}//sg;  #not needed in translation

  $tr=~s/\[\[[^\]]*?\|([^\]]*?)\]\]/$1/g; # remove wiki links with double-names
  $tr=~s/\[\[([^\]]*?)\]\]/$1/g; # remove other wiki-links

  $tr=~s/^\s*\n//sg; # remove all empty lines before entry
  $tr=~s/\n\s*$//sg; # remove all empty lines after entry

  while ($tr=~s/^\#\s*\n//s) {} # remove all empty list items before entry
  while ($tr=~s/\n\#\s*$//s) {}  # remove all empty list intems after entry

  return $tr;
}

# Cleanup translation from Russian into other laguages, that have been found in {{перев-блок template in "Перевод" section
sub _native_tr_cleanup
{
  my $self = shift;
  my $line = shift;
  my $page_name = shift;
  my $lang = shift;

  $line =~ s/\{\{[fmn]\}\}//gs; # Cleanup {{m}}, {{f}} and {{n}}

  $line=~s/\[\[[^\]]*?\|([^\]]*?)\]\]/$1/g; # remove wiki links with double-names
  $line=~s/\[\[([^\]]*?)\]\]/$1/g; # remove other wiki-links

  # process {{trad|es|anfibio}} {{trad|es}} templates, warn on language missmatch
  while ($line=~s/(\{\{trad\|([a-zA-Z0-9\-]*?)(?:\|(.*?)|)\}\})/$3/)
  {
    if ($2 ne $lang)
    {
      warn "Page $page_name has lang code mismatch. Translation $1 is located is located in {{базовый-блок}} laguage $lang";
    }
  }

  # process {{t|es|anfibio|n}} {{t|es|anfibio}} templates, warn on language missmatch
  while ($line=~s/(\{\{t\|([a-zA-Z0-9\-]*?)\|(.*?)(?:\|[a-z]|)\}\})/$3/)
  {
    if ($2 ne $lang)
    {
      warn "Page $page_name has lang code mismatch. Translation $1 is located is located in {{базовый-блок}} laguage $lang";
    }
  }

  return $line;
}

sub _parse_tr_single_line
{
  my $self = shift;
  my $line = shift;
  my $from_lang = shift;
  my $to_lang = shift;


  if ($line =~/\n\s*\n/s)
  {
    print $line;
    return "Not a single line";
  }
  if ($line =~/^\#/m)
  {
    return "Ordered list is not allowed in single line";
  }
  $line=~s/^\s*//s;
  $line=~s/\s*$//s;
  $line=~s/\s+/ /gs;

  my $sense = new TEI::FreeDict::Sense();

  foreach (split /[\,\;]\s/, $line)
  {
    if ($from_lang ne $to_lang)
    {
      $sense->add_translation($_);
    } else
    {
      # If it is [same-lang]-[same-lang] transtation, then it is not a translation, but definition
      $sense->add_definition($_);
    }
  }
  return $sense;
}

sub _parse_tr_numbered_list
{
  my $self = shift;
  my $tr = shift;
  my $from_lang = shift;
  my $to_lang = shift;

  my @lines = split /\n/,$tr;
  my @senses =();
  foreach my $line (@lines)
  {
    if (! ($line=~ /^\#\s?(.*)$/))
    {
      die "No '#' at the begining of the line in ordered list on page '".$self->title."' section '".$self->word_lang. "'. Translation seems to be too complex";
    }
    $line =~ /^\#\s?(.*)$/;
    $line = $1;
    my $sense = $self->_parse_tr_single_line($line, $from_lang, $to_lang);
    if (! (ref $sense))
    {
      die "Error parsing ordered list on page ".$self->title." section '".$self->word_lang."'";
    }
    push @senses, $sense;
  }
  return @senses;
}

sub translation
{
  #FIXME depricated
  my $self = shift;
  $self->translation_as_TEI(@_);
}

sub translation_as_TEI
{
  my $self = shift;
  my $lang = shift;
  
  my $translations = $self->all_translations();
  
  return $translations->{$lang} if  $translations->{$lang};
  die "There is no translation into language '$lang' in article '",$self->title, "' at section '", $self->word_lang,"'";
}


1;