package Wiktionary::Ru::Page;
use base 'Wiktionary::Page';

use Wiktionary::Section;


use strict;
use warnings;
use utf8;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {};
  bless $self,$class;
  return $self;
}

sub _split_on_sections
{
  my $self = shift;
  my $src = $self->{_body};
  $src = page_cleanup($src);
  $self->{_sections}={};

  my $prev_header = undef;
  my $prev_lang = undef;

  # Ru.Wiktionary uses ISO 639 codes for languages that are avialable. 2 letters code if it is available,
  # Or 3-letters for others.
  # For languages, that are not in ISO 639 it uses codes from http://multitree.org/codes/ (http://web.archive.org/web/20190406234153/http://multitree.org/codes/)
  # it is generally codeverd by [a-z0-9\-]{2,}? regexp
  # -INT- came from old times, but still used, and now is redirection to -mul-
  # -Latn- is header for Latin script, not language. See  https://ru.wiktionary.org/wiki/v#Латиница as example
  while ($src =~ s/^(.*?)(\=\s*\{\{\-([a-z0-9\-]{2,}?|INT|Latn)\-(?:\|.*?|)\}\}\s*?\=)//s)
  {
    my $text = $1;
    my $next_header = $2;
    my $next_lang = $3;
    if (! defined $prev_header )
    {
      # If we have not seen any header before then then it is "prefix", text that goes before the first hader
      $self->{_prefix} = $text;
      $self->{_prefix} = prefix_cleanup($self->{_prefix});
      if (! ($self->{_prefix}  =~ /^\s*$/s)) { warn "Page ".$self->{_title}." has non emply prefix '".$self->{_prefix}."', prehaps wrong formatting"}
    } else
    {
      if ($self->{_sections}->{$next_lang}) { die "Page ".$self->{_title}." has more then one entry of $next_lang language title"; }
      $self->{_sections}->{$prev_lang}= new Wiktionary::Section(page => $self, header => $prev_header, body => $text, word_lang => $prev_lang);
    }
    $prev_header = $next_header;
    $prev_lang = $next_lang;
  }
  if (! defined $prev_header) {die "Page ".$self->{_title}." does not have proper language header " }
  # Add the tail of $src as last section
  $self->{_sections}->{$prev_lang}= new Wiktionary::Section(page => $self, header => $prev_header, body => $src, word_lang => $prev_lang);
  return 1;
}

# Thare are elements, that are not actually parsed (at least for now)
# but that can appeare in any part of page, and spoil parsing we actually do
# so we clean them up
sub page_cleanup
{
  my $text = shift;

  return $text;
}

# Cleans prefix (an unnamed section at the beggining of the page) from known, but unused template-includes.
sub prefix_cleanup
{
  my $text = shift;
  $text =~s/\{\{[Cc]f\|.*?\}\}//gs;  # REMOVE "see also" template from the beginning of the page and make shure that what is left is empty
  $text =~s/\{\{сф\|.*?\}\}//gs;  # Same as Cf

  # {{См|activación}} https://ru.wiktionary.org/wiki/activation
  $text =~s/\{\{См\|.*?\}\}//gs; #Another version of see also template
  $text =~s/\{\{Cf2\|.*?\}\}//gs;
  $text =~s/\{\{also\|.*?\}\}//gs;

  $text =~s/\{\{wikify\}\}//sg;
  $text =~s/\{\{offensive(?:\|.*?|)\}\}//sg; #FIXME this might be imported later

  $text =~s/\{\{wikipedia(?:\|.*?|)\}\}//sg; # Remove template that links wikipedia
  $text =~s/\{\{Википедия(?:\|.*?|)\}\}//sg;
  $text =~s/\{\{викивиды\|.*?\}\}//sg;

  $text =~s/\{\{commons(?:\|.*?|)\}\}//sg;


  $text =~s/\{\{Сводеш-200\|.*?\}\}//gs;
  $text =~s/\{\{иноязычное слово дня\|.*?\}\}//gs; # Do not need it too
  $text =~s/\{\{статья недели\|.*?\}\}//gs;
  $text =~s/\{\{слово дня\|.*?\}\}//gs;

   return $text;
}


#sub sections_langs
#{
#  my $self = shift;
#  if (! %{$self->{_sections}})
#  {
#    $self->_split_on_sections;
#  }
#  return keys %{$self->{_sections}};
#}

#sub get_section
#{
#  my $self = shift;
#  my $lang = shift;
#  if (! %{$self->{_sections}})
#  {
#    $self->_split_on_sections;
#  }
#  return $self->{_sections}->{$lang};
#}


1;