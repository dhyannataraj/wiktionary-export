package Wiktionary::Section;

use strict;
use warnings;
use Carp qw(cluck);
 
use Scalar::Util qw( weaken );


sub new
{
  my $class = shift;
  my %opt = @_;
  
  my $page = $opt{page};
  my $self = undef;
  
  if ($page->lang eq 'ru')
  {
    use Wiktionary::Ru::Section;
    $self = new Wiktionary::Ru::Section(%opt);
  }
  if ($page->lang eq 'en')
  {
    use Wiktionary::En::Section;
    $self = new Wiktionary::En::Section(%opt);
  }
  $self->_init(%opt) if $self;
  return $self;
}

sub _init
{
  my $self = shift;
  my %opt = @_;
  
  $self->{_page} = $opt{page};
  $self->{_word_lang} = $opt{word_lang};
  
  weaken($self->{_page});
  $self->{_server} = $self->{_page}->server;
  weaken($self->{_server});
  
  $self->{_title} = $self->{_page}->title;
}

sub title
{
  my $self = shift;
  return $self->{_title};
}

sub page
{
  my $self = shift;
  return $self->{_page};
}

sub server
{
  my $self = shift;
  return $self->{_server};
}

sub page_lang
{
  my $self = shift;
  return $self->page->lang;
}

sub lang
{
  cluck "Wiktionary::Section::lang is depricated!";
  my $self = shift;
  return $self->page->lang;
}



sub section_lang
{
  cluck "Wiktionary::Section::section_lang is depricated!";
  my $self = shift;
  return $self->{_word_lang};
}

sub word_lang
{
  my $self = shift;
  return $self->{_word_lang};
}


sub articles
{
  my $self = shift;
  if (! @{$self->{_articles}})
  {
    $self->_split_on_articles;
  }
  return @{$self->{_articles}};
}

#sub DESTROY 
#{
#  print "destroing section\n";
#}

1;