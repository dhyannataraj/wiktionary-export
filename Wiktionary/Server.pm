package Wiktionary::Server;

use strict;
use utf8;

use Wiktionary::Page;
use MediaWiki::API;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self;
  if ($opt{lang} eq 'ru')
  {
    use Wiktionary::Ru::Server;
    $self = new Wiktionary::Ru::Server(%opt);
  }
  if ($opt{lang} eq 'en')
  {
    use Wiktionary::En::Server;
    $self = new Wiktionary::En::Server(%opt);
  }
  if ($self)
  {
    $self->{_debug}=1 if $opt{debug};
    return $self;
  }
  return undef;
}

sub get_word_list
{
  my $self = shift;
  my $lang = shift;

  my $category = $self->{'_language_catigories'}->{$lang};
  if ($category)
  {
#    my $mw = MediaWiki::API->new();
#    $mw->{config}->{api_url} = $self->{_api_url};
    my $mw=$self->_mw_api_engine();

    my $articles = $mw->list ( {
	rawcontinue=>1, # FIXME Hotfix https://www.mediawiki.org/wiki/API:Raw_query_continue
	action => 'query',
	list => 'categorymembers',
	cmtitle =>  $category,
	cmnamespace => 0, # Common articles, not subcatigories
	cmlimit => 'max' } , {skip_encoding => 1} 
    ) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
    my @res =();
    foreach (@$articles)
    {
      push @res,$_->{title};
    }
    return @res;
  }
  print STDERR "Unknown language = '$lang'\n";
  die;
}

sub get_page
{
  my $self = shift;
  my $title = shift;
  
  my $mw = $self->_mw_api_engine;
  my $page = $mw->get_page( { title => $title }, {skip_encoding => 1} ) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
  my $page_body = $page->{'*'};

  my $page = new Wiktionary::Page(server=>$self, title=>$title, body => $page_body);
#  $page->_parse_src;
  
  return $page;
}


sub lang
{
  my $self = shift;
  return $self->{_lang};
}

sub debug
{
  my $self = shift;
  my $value = shift;

  $self->{_debug} = $value if defined $value;
  return $self->{_debug};
}

sub _mw_api_engine
{
  my $self = shift;
  if (! $self->{_mw_api_engine})
  {
    $self->{_mw_api_engine} = MediaWiki::API->new();
    $self->{_mw_api_engine}->{config}->{api_url} = $self->{_api_url};
  }
  return $self->{_mw_api_engine};
}


#sub DESTROY
#{
#  print "destroing server\n";
#}

1;
