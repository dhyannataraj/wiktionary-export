
use Test::More;
use strict;
BEGIN { plan tests => 11 }; 


my $good_page = 
'= d =
D D D D D D D D D
=== ee ===
E EE EEEE E
== ff ==
FFF F FF F F F
== gg ==
G G GGG
= h =
HHHH
';

use_ok('MediaWiki::Parser');  #1


my $parser = new MediaWiki::Parser();
ok($parser);                            #2
is(ref($parser), 'MediaWiki::Parser');  #3

# $parser->debug(1);

my $WOM = $parser->parse($good_page);
ok($WOM);                                      #4
is (ref($WOM),'MediaWiki::Parser::Document');  #5

my @l = $WOM->subsections_by_title('h');
is($#l,0);  #6
is($l[0]->body_as_string,"HHHH\n");  #7
is($l[0]->level,1);  #8

 @l = $WOM->subsections_by_title('ff');
is($#l,0);  #9
is($l[0]->body_as_string,"FFF F FF F F F\n");  #10
is($l[0]->level,2);  #11

