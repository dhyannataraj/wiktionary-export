use Test::More;
use Test::Exception;
use strict;
use utf8;

BEGIN { plan tests => 125 };

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

# This is needed for Data::Dumper to print utf8 strings
use Data::Dumper;
$Data::Dumper::Useperl = 1;
$Data::Dumper::Useqq = 1;
{
  no warnings 'redefine';
  sub Data::Dumper::qquote {
    my $s = shift;
    return "'$s'";
  }
}


my $current_dir = $0;
$current_dir = '.' if ! ($current_dir=~/\//);
$current_dir =~ s/(\/.*?$)//;

use_ok('Wiktionary::Page');  #1

my $page;
my $section;
my @articles;
my $translation;
my @senses;
my @values;

ok($page = new Wiktionary::Page(lang=>'ru'));           #2
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/commode.wiki", 'commode')); #3
is(scalar($page->get_section('en')->articles),1);       #4
is(scalar($page->get_section('ia')->articles),1);       #5
is(scalar($page->get_section('nl')->articles),1);       #6
is(scalar($page->get_section('fr')->articles),2);       #7
is(scalar($page->get_section('do-not-exists')),undef);  #8

ok(@articles = $page->get_section('fr')->articles);     #9
ok($translation = $articles[0]->all_translations->{ru}); #10
is($translation->form->title,'commode');                #11
is($translation->form->pronunciation,"kɔ.ˈmɔd");        #12
ok(@senses = $translation->senses);                     #13
is(scalar(@senses),2);                                  #14
ok(@values = $senses[0]->translations);			#15
is(scalar(@values),1);					#16
is($values[0]->value,"удобный");			#17

ok(@values = $senses[1]->translations);			#18
is(scalar(@values),2);					#19
is($values[0]->value,"простой");			#20
is($values[1]->value,"лёгкий");				#21

# ########### Testing {{PAGENAME}} as section title
ok($page = new Wiktionary::Page(lang=>'ru'));		#22
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/abstrait.wiki", 'abstrait'));  #23
ok($section = $page->get_section('fr'));		#24
ok(@articles = $section->articles);			#25
is($#articles,1); # there are two articles in this page	#26

# ########### Testing Союз as section title
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/si.wiki", 'si'));  #27
ok($section = $page->get_section('es'));		#28
ok(@articles = $section->articles);			#29
is($#articles,1); # there are two articles in this page	#30


# ########### Testing {{заголовок[...]}} as section title
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/mine.wiki", 'mine'));  #28
ok($section = $page->get_section('fr'));		#29
ok(@articles = $section->articles);			#30
is($#articles,3); # there are four articles in this page	#31


# ########### Testing that articles with {{plur| template are ignored

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/mole.wiki", 'mole'));  #32
ok($section = $page->get_section('it'));		#33
ok(@articles = $section->articles);			#34
is($#articles,1); # there are two articles in this page	#35


# ########### Testing that articles with {{{forms| template are ignored

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/esclavage.wiki", 'esclavage'));  #36
ok($section = $page->get_section('fr'));		#37
ok(@articles = $section->articles);			#38
is($#articles,0); # there is one article in this page	#39


# ############ Test that new =={Заголовок|II}== template works

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/acustica.wiki", 'acustica'));  #40
ok($section = $page->get_section('it'));		#41
ok(@articles = $section->articles);			#42
is($#articles,1); # there are two articles in this page	#43

# ############ Test that we properly ignore articles with {{словоформа template in it

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/cessi.wiki", 'cessi'));  #44
ok($section = $page->get_section('it'));		#45
ok(@articles = $section->articles);			#46
is($#articles,0); # there are one article in this page	#47

# ############ Test that new =={з|II}== template works

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/calyptre.wiki", 'calyptre'));  #48
ok($section = $page->get_section('fr'));		#49
ok(@articles = $section->articles);			#50
is($#articles,1); # there are two articles in this page	#51

# ############ Check that [[Файл:....]] links are properly ignored

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/abraxas.wiki", 'abraxas'));  #52
ok($section = $page->get_section('fr'));		#53
ok(@articles = $section->articles);			#54
is($#articles,1); # there are two articles in this page	#55
ok($translation = $articles[1]->all_translations->{ru}); #56

# ############ Check that == {{заголовок|(существительное)}} == is properly paresd 

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/personne.wiki", 'personne'));  #57
ok($section = $page->get_section('fr'));		#58
ok(@articles = $section->articles);			#49
is($#articles,1); # there are two articles in this page	#60

# ############ Check that == {{заголовок|(существительное I)}} == is properly paresd 

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/ardoise.wiki", 'ardoise'));  #57
ok($section = $page->get_section('fr'));		#58
ok(@articles = $section->articles);			#49
is($#articles,2); # there are three articles in this page	#60

# ############ Check that '# {{Нужен перевод|fr}} {{пример||перевод=}}' is treated as empty translation
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/abakwariga.wiki", 'abakwariga'));  #61
ok($section = $page->get_section('fr'));		#52
ok(@articles = $section->articles);			#53
throws_ok {$articles[0]->all_translations->{ru}} qr/Section 'Значение' is empty on page 'abakwariga', section 'fr', subsection ''/; #64


# ############ Check that '{{семантика|..}}' template is properly ignored
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/trop.wiki", 'trop')); #65
ok(@articles = $page->get_section('fr')->articles);     #66
ok($translation = $articles[0]->all_translations->{ru}); #67
ok(@senses = $translation->senses);                     #68
is(scalar(@senses),1);                                  #69

ok(@values = $senses[0]->translations);			#70
is(scalar(@values),2);					#71
is($values[0]->value,"слишком");			#72
is($values[1]->value,"чересчур");			#73


# ############ Check that empty  {{пример||*}} is ignored
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/abbesse.wiki", 'abbesse')); #74
ok(@articles = $page->get_section('fr')->articles);     #75
ok($translation = $articles[0]->all_translations->{ru}); #76
ok(@senses = $translation->senses);                     #77
is(scalar(@senses),1);                                  #78

ok(@values = $senses[0]->translations);			#79
is(scalar(@values),2);					#80
is($values[0]->value,"аббатиса");			#81
is($values[1]->value,"настоятельница");			#82

# Check that &#32; seq is cleaned after remplates
# Also check that {{помета.}} template is ignored
ok($page->load_from_file("$current_dir/ru.wiktionary.pages/lyrique.wiki", 'lyrique')); #83
my $tr = _my_get_translations(page => $page, lang_from => 'fr', lang_to => 'ru', expected_articles =>1, expected_senses => 1); # 84.. 89
is_deeply($tr,  [['лирический']]); #90

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/беспардонный.wiki", 'беспардонный')); #91
my $tr = _my_get_translations(page => $page, lang_from => 'ru', lang_to => 'it', expected_articles =>1, expected_senses => 1); # 92.. 97
is_deeply($tr,  [['impudente','insolente','sfacciato','sfrontato']]); #98


ok($page->load_from_file("$current_dir/ru.wiktionary.pages/замок.wiki", 'замок')); #99
my $tr = _my_get_translations(page => $page, lang_from => 'ru', lang_to => 'en', article_n => 1, expected_articles =>3, expected_senses => 3); # 100.. 105
is_deeply($tr,  [['lock'],['lock'],['keystone']]); #106

ok($page->load_from_file("$current_dir/ru.wiktionary.pages/decimo.wiki", 'decimo')); #107
my $tr = _my_get_translations(page => $page, lang_from => 'la', lang_to => 'ru', article_n => 0, expected_articles =>2, expected_senses => 3); # 108.. 113
is_deeply($tr,  [['наказывать или казнить каждого десятого','децимировать'],['облагать десятиной'],['отбирать в качестве десятины']]); #114


ok Wiktionary::Ru::Section::check_zagolovok_template("(Глагол)");
ok !Wiktionary::Ru::Section::check_zagolovok_template("(Фигня)");
ok Wiktionary::Ru::Section::check_zagolovok_template("(Глагол XIV)");
ok Wiktionary::Ru::Section::check_zagolovok_template("VI");
ok !Wiktionary::Ru::Section::check_zagolovok_template("(Глагол XIVZZ)");
ok !Wiktionary::Ru::Section::check_zagolovok_template("VIZZ");
ok Wiktionary::Ru::Section::check_zagolovok_template("I (существительное)");
ok !Wiktionary::Ru::Section::check_zagolovok_template("I (фигня)");

sub _my_get_translations
{
  my %opt = @_;
  my $page = $opt{page};
  my $lang_from = $opt{lang_from};
  my $lang_to = $opt{lang_to};
  my $article_n = $opt{article_n} || 0;
  my $expected_articles = $opt{expected_articles};
  my $expected_senses = $opt{expected_senses};
  
  my @articles;
  ok(@articles = $page->get_section($lang_from)->articles);                # +1

  is(scalar @articles, $expected_articles) if defined $expected_articles;  # +2(?)
  my $translation;
  ok($translation = $articles[$article_n]->all_translations->{$lang_to});  #+3
  ok(@senses = $translation->senses);                                      #+4
  is(scalar(@senses),$expected_senses) if defined $expected_senses;        #+5(?)

  my $res=[];
  foreach my $sense (@senses)
  {
    my $v = [];
    foreach my $value ($sense->translations)
    {
      push @$v,$value->value;
    }
    push @$res, $v;
  }
  return $res;
}
