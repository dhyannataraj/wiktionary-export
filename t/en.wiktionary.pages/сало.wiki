==Macedonian==

===Etymology===
From {{proto|Slavic|sadlo|lang=mk}}.

===Pronunciation===
* {{IPA|/ˈsalɔ/|lang=mk}}

===Noun===
{{mk-noun|g=n|tr=sálo|plural|сала|sc=Cyrl}}

# [[fat]]
# [[grease]]
# [[lard]]

====Synonyms====
* [[маст]]

[[Category:mk:Food and drink]]

----

==Russian==
{{wikipedia|lang=ru}}

===Etymology===
From {{proto|Slavic|sadlo|lang=ru}}.

===Noun===
'''сало''' (sálo) {{n}}

# [[suet]], [[fat]], [[lard]]
# [[tallow]]

====Declension====
{{ru-noun-inan-о-1|сал|са́л}}

====Related terms====
* [[салить]], [[засалить]]
* [[сальник]]
* [[сальный]]

[[Category:Russian nouns]]
[[Category:ru:Food and drink]]

----

==Serbo-Croatian==

===Etymology===
From {{proto|Slavic|sadlo|lang=sh}}.

===Pronunciation===
* {{IPA|/sâlo/|lang=sh}}
* {{hyphenation|са|ло}}

===Noun===
{{sh-noun|g=n|head=са̏ло|c|salo|sȁlo}}

# [[lard]]
# [[fat]] (specialized animal tissue with a high oil content, used for long-term storage of energy)

====Declension====
{{sh-decl-noun|sc=Cyrl
|са̏ло|сала
|сала|са̑ла̄
|салу|салима
|сало|сала
|сало|сала
|салу|салима
|салом|салима
}}

[[bg:сало]]
[[et:сало]]
[[el:сало]]
[[fr:сало]]
[[ko:сало]]
[[pl:сало]]
[[ru:сало]]
[[fi:сало]]
[[tr:сало]]
[[uk:сало]]
[[vi:сало]]
[[zh:сало]]

