==English==

===Etymology===
From {{etyl|fro}} {{term|mangeoire||lang=fro}}, {{term|manjoire||lang=fro}}, from {{term|mangier||lang=fro|to eat}} (modern French ''[[#French|manger]]'').

===Pronunciation===
* {{audio|en-uk-manger.ogg|Audio (UK)}}
* {{IPA|/ˈmeɪndʒə(r)/}}, {{SAMPA|/"meIndZ@/}}
* {{hyphenation|man|ger}}
* {{rhymes|eɪndʒə(r)}}

===Noun===
{{en-noun}}

# A [[trough]] for animals to eat from.

====Derived terms====
* [[dog in the manger]]

====Translations====
{{trans-top|trough for animals to eat from}}
* [[Catalan]]: {{t|ca|pessebre|m}}
* Dutch: {{t-|nl|kribbe}}
* Finnish: {{t+|fi|kaukalo}}
* French: {{t+|fr|mangeoire|f}}
* Georgian: {{t|ka|ბაგა|sc=Geor|xs=Georgian}}
* German: {{t+|de|Krippe|f}}
{{trans-mid}}
* Hebrew: {{t+|he|אבוס|tr=avos}}
* Italian: {{t-|it|mangiatoia|f}}
* Korean: {{t-|ko|구유|tr=guyu|sc=Hang}}
* Portuguese: {{t|pt|manjedoura|f}}
* Spanish: {{t+|es|pesebre|m}}
* Turkish: {{t|tr|yemlik}}
{{trans-bottom}}

===Anagrams===
* [[engram#English|engram]], [[Engram#English|Engram]], [[german#English|german]], [[German#English|German]], [[ragmen#English|ragmen]]

----

==French==

===Etymology===
From {{etyl|fro|fr}} ''[[mangier]]'', from {{etyl|LL.|fr}} ''[[manducare]]'' ‘to chew, devour’, present active infinitive of {{term|manduco|manducō|lang=la}}.

===Pronunciation===
* {{IPA|/mɑ̃ʒe/|lang=fr}}, {{SAMPA|/ma~Ze/}}
* {{audio|Fr-manger.ogg|Audio (France)}}
* {{a|Paris}} {{IPA|[mɑ̃ː.ˈʒe]|lang=fr}}
* {{audio|Fr-manger-fr-FR-Paris.ogg|Audio (France, Paris)}}
* {{homophones|mangeai|mangé|mangée|mangées|mangés|mangez|lang=fr}}
* {{hyphenation|man|ger}}

===Verb===
{{fr-verb}}

# {{transitive|lang=fr}} To [[eat]].
#* '''''Manger''' du pain, de la viande.''
# {{intransitive|lang=fr}} To [[eat]].
#* '''''Manger''' peu, beaucoup, trop.''
#* ''Manger au restaurant.''

====Conjugation====
{{fr-conj-ger|man|avoir}}

===Noun===
{{fr-noun|m}}

# [[food]], [[foodstuff]].
#: ''Un '''manger''' délicat.''

===Anagrams===
* [[magner#French|magner]]

----

==Middle French==

===Verb===
{{infl|frm|verb}}

# to [[eat]] {{gloss|consume food}}

====Conjugation====
{{frm-conj-ger|man}}

====Coordinate terms====
* {{l|frm|boire}}, {{l|frm|boyre}}

[[ca:manger]]
[[cs:manger]]
[[de:manger]]
[[et:manger]]
[[el:manger]]
[[es:manger]]
[[eo:manger]]
[[fa:manger]]
[[fr:manger]]
[[gl:manger]]
[[ko:manger]]
[[hr:manger]]
[[io:manger]]
[[id:manger]]
[[it:manger]]
[[kn:manger]]
[[ka:manger]]
[[sw:manger]]
[[lv:manger]]
[[lb:manger]]
[[lt:manger]]
[[li:manger]]
[[hu:manger]]
[[mg:manger]]
[[ml:manger]]
[[my:manger]]
[[nl:manger]]
[[ja:manger]]
[[no:manger]]
[[oc:manger]]
[[pl:manger]]
[[pt:manger]]
[[ro:manger]]
[[ru:manger]]
[[fi:manger]]
[[sv:manger]]
[[ta:manger]]
[[te:manger]]
[[tr:manger]]
[[vi:manger]]
[[vo:manger]]
[[zh:manger]]

