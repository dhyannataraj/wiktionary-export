package MediaWiki::Parser::Section;

use strict;
use Scalar::Util qw( weaken ); 

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {_elements => [], _subsections=>[], _title => $opt{title}, _level => $opt{level}};
  
  bless $self, $class;
  return $self;
}

sub push_element
{
  my $self = shift;
  my $element = shift;
  
  $element->{_parent} = $self;
  weaken($element->{_parent});
  
  push @{$self->{_elements}}, $element;
  if (ref $element eq 'MediaWiki::Parser::Section')
  {
    push @{$self->{_subsections}}, $element;
  }
}

sub parent
{
  my $self = shift;
  return $self->{_parent};
}

sub title
{
  my $self = shift;
  return $self->{_title};
}

sub level
{
  my $self = shift;
  return $self->{_level};
}

sub subsections_by_title
{
  my $self = shift;
  my $title = shift;
  my @res = ();
  foreach my $sec (@{$self->{_subsections}})
  {
    if ($sec->title eq $title)
    {
      push @res, $sec;
    }
    push @res, $sec->subsections_by_title($title);
  }
  return @res;
}

sub as_string
{
  my $self = shift;
  
  my $str = "=" x $self->level;  # FIXME this does not preserve source text formatting, should be rewritten
  $str = "$str ".$self->title." $str\n";
  $str .= $self->body_as_string;
  return $str;
}

sub body_as_string
{
  my $self = shift;
  my $str = "";
  foreach (@{$self->{_elements}})
  {
    $str.=$_->as_string;
  }
  return $str;
}

sub DESTROY
{
  my $self = shift;
  delete $self->{_parent}; #preventing circular links
}
1;