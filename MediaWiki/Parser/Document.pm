package MediaWiki::Parser::Document;

use strict;
use Scalar::Util qw( weaken );

sub new
{
  my $class = shift;
  my $self = {_elements => [], _subsections=>[]};

  bless $self, $class;
  return $self;
}

sub push_element
{
  my $self = shift;
  my $element = shift;
  $element->{_parent} = $self;
  weaken($element->{_parent});
  
  push @{$self->{_elements}}, $element;
  if (ref $element eq 'MediaWiki::Parser::Section')
  {
    push @{$self->{_subsections}}, $element;
  }
}

sub subsections_by_title
{
  my $self = shift;
  my $title = shift;
  my @res = ();
  foreach my $sec (@{$self->{_subsections}})
  {
    if ($sec->title eq $title)
    {
      push @res, $sec;
    }
    push @res, $sec->subsections_by_title($title);
  }
  return @res;
}

1;