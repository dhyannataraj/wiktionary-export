package MediaWiki::Parser::Paragraph;

use strict;
use warnings;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {};
  $self->{_text} = $opt{'text'} if $opt{text};

  bless $self, $class;
  return $self;
}

sub as_string
{
  my $self = shift;
  return $self->{_text};
}

sub _append_line
{
  my $self = shift;
  my $line = shift;
  $self->{_text} = '' unless defined $self->{_text};
  $self->{_text}.=$line;
}

1;