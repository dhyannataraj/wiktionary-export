package MediaWiki::Parser;

use strict;

use MediaWiki::Parser::Document;
use MediaWiki::Parser::Section;
use MediaWiki::Parser::Paragraph;

sub new
{
  my $class = shift;
  my %opt = @_;
  my $self = {};
  $self->{_debug}=1 if $opt{debug};

  bless $self, $class;
  return $self;
}

sub debug
{
  my $self = shift;
  my $value = shift;

  $self->{_debug} = $value if defined $value;
  return $self->{_debug};
}

sub parse
{
  my $self = shift;
  my $text = shift;
  my $document = new MediaWiki::Parser::Document;
  
  print STDERR "parsing the following text:\n==========================\n$text\n=========================\n" if $self->debug;
  
  my $current_section = $document;
  my $current_paragraph = undef;

  my $line_cnt = 0;
  foreach my $line (split /\n/,$text)
  {
    $line_cnt++;
    $line =~ /(^.)/;
    my $first_char = $1;
    $first_char = undef if length($line) == 0;
    
    if ($first_char && $first_char eq "=")
    {
      print STDERR "parser meets Header $line at line $line_cnt\n" if $self->debug;
      
      $line =~/^(=+)\s*(.+?)\s*(=+)(.*)$/;
      my $open_tag = $1;
      my $close_tag = $3;
      my $title = $2;
      my $trailing_spaces = $4;
      if ($trailing_spaces && ( !($trailing_spaces =~ /^\s*$/)))
      {
        die "Some garbige ($trailing_spaces) at the end of section title $title";
      }
      if ($open_tag ne $close_tag)
      {
        warn "FIXME: assimetric header tag parsing is not implemented yet, level of the header might be wrong!"
      }
      my $level = length($open_tag);
      $level = length($close_tag) if length($close_tag) < $level;
      
      die "Section title tag were not closed at line $line_cnt. Aborting" if $level==0;

      while ($current_section && (ref($current_section) eq 'MediaWiki::Parser::Section') && ($current_section->level>=$level))
      {
        $current_section=$current_section->parent;
      }
      $current_paragraph = undef;
      my $section = new MediaWiki::Parser::Section(title => $title, level=> $level);
      $current_section->push_element($section);
      $current_section = $section;
    } else
    {
      unless ($current_paragraph)
      {
        $current_paragraph = new MediaWiki::Parser::Paragraph(text => '');
        $current_section->push_element($current_paragraph);
      }
      $current_paragraph->_append_line($line."\n");
    }
  }
  return $document;
}

1;