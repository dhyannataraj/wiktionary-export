#!/usr/bin/perl

use strict;
use utf8;

use Wiktionary::Server;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

# This is needed for Data::Dumper to print utf8 strings
use Data::Dumper;
$Data::Dumper::Useperl = 1;
$Data::Dumper::Useqq = 1;
{
  no warnings 'redefine';
  sub Data::Dumper::qquote {
    my $s = shift;
    return "'$s'";
  }
}

my $word = shift @ARGV;
Encode::_utf8_on($word);

die "Please specify a word as an argument" unless $word;

my $server = new Wiktionary::Server(lang=>'ru'); 
my $page = $server->get_page($word);

foreach my $lang ($page->sections_langs)
{
  print "========= $lang =========\n";
  my $section;
  eval {$section  = $page->get_section($lang)};
  if ($@)
  {
    print " ERROR: $@";
    next;
  }
  foreach my $article ($section->articles)
  {
    print "-------------------\n";
    my $res;
    eval {$res = $article->all_translations};
    if ($@)
    {
      print " ERROR: $@";
      next;
    }
    print ref $article,"\n";
    my $trans = $article->all_translations();
    print "<<Translation is empty>>\n" unless $trans;
    if ($trans)
    {
      foreach my $to_lang (keys %$trans)
      {
        print "-- $to_lang --\n";
        print $trans->{$to_lang}->asString;
      }
    }
  }
}

#use Data::Dumper;
#print Dumper $page;

# my $page->get_section($from_lang)->articles;
