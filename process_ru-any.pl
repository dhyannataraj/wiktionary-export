#!/usr/bin/perl
# a script to get words in a given language from the russian
# lnaguage wiktionary server

use utf8;
use strict;
use warnings;
binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';
use Encode;
#use Class::Unload;

use FindBin;
use lib $FindBin::Bin."/.";

use Wiktionary;
use Wiktionary::Server;
use Wiktionary::Page;

use Lingua::StarDict::Writer;
use TEI::FreeDict;

use Libredict::En;

use Data::Dumper;

$Data::Dumper::Terse = 1; # Do not print var name
$Data::Dumper::Indent = 0; # Do skip all spaces (human unreadable)


my @lang_to_publish=qw(ru en de fr es it uk eo pl cs nl bg pt be la fi sv tr el hr ko da mi lv sah chm tt ba os myv cv ce kk);
my $dict_path = "/.www-data/nataraj/wiktionary-export/dictionaries/";
my $local_work = 0;  # Set to 1 for experemental run, that does not put the result in the web.

$dict_path = 'dict_test' if $local_work;


my %langcodes_seen = (); #Langage codes that were met by parsing script

`rm -rf output`;
`mkdir -p output`;

my ($year,$mon,$mday)= date();

my $server = new Wiktionary::Server('lang'=>'ru');

my @list=();
if ($local_work && (-e "ru.list"))
{
  print "Using manual list\n";
  open F, "<:utf8","ru.list";
  while (my $s=<F>)
  {
    chomp $s;
    push @list, $s;
  }
  close F;
} else
{
  print "Getting word list...\n";

  @list = $server->get_word_list('ru');
}

my $count = 0;
foreach my $word (@list)
{
  print "Processing $word...\n";
  $count++;

  my $page = undef;

# Socket::IO leaks memory now. MediaWiki::API ises it via LWP, while fetching page.
# So it consumes all available memory for long run. So instead of calling MediaWiki::API we will run
# an external script, that fethes a page and then frees all leaked memory when process is terminated
# When Socket::IO memory leak would ble complealry fixed, we will be able to call $server->get_page again.

#  eval {$page = $server->get_page($word);};

  my $escaped_word = escape_quotes($word);
  my $page_text = `./fetch_ru_wiktionary_page.pl "$escaped_word"`;
  $page_text = Encode::decode_utf8($page_text);
  if ($?)
  {
    write_log( "Error fetching page '$word'");
    next;
  }
  $page = new Wiktionary::Page(server=>$server, title=>$word, body => $page_text);



#eval{$page = new Wiktionary::Page(server=>$server,title=>$word);
#$page->load_from_file("$word.wiki");};


#  if ($@)
#  {
#    my $error_msg = $@;
#    write_log($word,$error_msg);
#  } else
  {
    foreach ('ru')
    {
      my $from_lang = $_;
      eval {$page->get_section($from_lang)->articles;};
      if ($@)
      {
        my $error_msg = $@;
        write_log($word,$error_msg);
      } else
      {
        my $sec=$page->get_section($from_lang);
        foreach my $art ($sec->articles())
        {
          eval {$art->all_translations};
          if ($@)
          {
            my $error_msg = $@;
            write_log($word,$error_msg);
          } else
          {
            my $all_trans = $art->all_translations;
            foreach my $to_lang (keys %{$all_trans})
            {
              $langcodes_seen{$to_lang} = 1;
              open(F ,">>:utf8","./output/$to_lang");
              my $ss = Dumper($all_trans->{$to_lang});
              $ss =~ s/\n/\\n/gs;
              print F "$ss\n";
              close F;
            }
          }
        }
      }
    }
  }
}

#foreach (split /\n/,`ls output.copy`)
#{
#  $dump_fh->{$_}=1;
#}


my %extents=();
foreach my $lang_code (@lang_to_publish)
{
  if (! -e "./output/$lang_code")
  {
    $extents{$lang_code}=0;
    next;
  }
  open F, "<:utf8","./output/$lang_code";
#  open F, "<:utf8","./output.copy/$lang_code";
  my %title_hash=();
  my @l=();
  while ( my $s=<F>)
  {
     my $el;
     eval("\$el=$s;");
     $title_hash{$el->form->title}=1;
     push @l, $el;
  }
  $extents{$lang_code} = scalar(keys(%title_hash));


  my $TEI = new_TEI_dict($lang_code, scalar(keys(%title_hash)));
  my $star = new_star_dict($lang_code);
  open PLAIN, ">:utf8",  "output/wiktionary_ru-${lang_code}_latest_plaintext.txt";
  foreach my $tei_entry (@l)
  {
    $TEI->add_entry($tei_entry);
    $star->entry($tei_entry->form->title)->add_part(type=> 't', data => $tei_entry->form->pronunciation) if $tei_entry->form->pronunciation;
    $star->entry($tei_entry->form->title)->add_part(type=> 'm', data => $tei_entry->asPlainTextEntry({skip_headword => 1, skip_pronunciation => 1}) );

    print PLAIN $tei_entry->asPlainTextEntry,"\n";
    print PLAIN "--------------------\n";
  }
  close PLAIN;
  open TEI, ">:utf8", "output/wiktionary_ru-${lang_code}_$year-$mon-$mday.tei";
  print TEI $TEI->asString;
  close TEI;
  $star->write;
  close F;

  `mkdir -p ${dict_path}/ru-$lang_code`;

  `gzip output/wiktionary_ru-${lang_code}_$year-$mon-$mday.tei`;
  `mv output/wiktionary_ru-${lang_code}_$year-$mon-$mday.tei.gz ${dict_path}/ru-$lang_code`;

  `gzip output/wiktionary_ru-${lang_code}_latest_plaintext.txt`;
  `mv  output/wiktionary_ru-${lang_code}_latest_plaintext.txt.gz ${dict_path}/ru-$lang_code`;


  my $lang_name = Libredict::En::lang_name($lang_code);

  `cd output; tar -cvzf wiktionary_ru-${lang_code}_stardict_$year-$mon-$mday.tgz 'Wiktionary Russian-$lang_name'`;
  `mv output/wiktionary_ru-${lang_code}_stardict_$year-$mon-$mday.tgz ${dict_path}/ru-$lang_code`;
  `rm -r 'output/Wiktionary Russian-$lang_name'`;

  `echo "$year-$mon-$mday" >  ${dict_path}/ru-$lang_code/last_release_date`;
  `echo "$extents{$lang_code}" >  ${dict_path}/ru-$lang_code/last_release_word_count`;
}

open F,">","output/stat";
foreach (sort( {$extents{$b} <=> $extents{$a}} @lang_to_publish))
{
  print F "$_\t".($extents{$_}||'')."\t".Libredict::En::lang_name($_)."\n";
}
close F;


sub new_star_dict
{
  my $lang_code = shift;
  my ($year,$mon,$mday)= date();
  my $lang_name = Libredict::En::lang_name($lang_code);

  my $stardict_writer = new Lingua::StarDict::Writer (name=>"Wiktionary Russian-$lang_name", date=>"$year-$mon-$mday", output_dir=>'./output/');
  return $stardict_writer;
}

sub new_TEI_dict
{
  my $lang_code = shift;
  my $extent = shift;
  my ($year,$mon,$mday)= date();
  my $lang_name = Libredict::En::lang_name($lang_code);

  my $TEI = new TEI::FreeDict;
  my $tei_header = new TEI::FreeDict::Header(
    title => "Wiktionary Russian-$lang_name", 
    edition => $year*0.0001+$mon*0.000001+$mday*0.00000001,
    extent => $extent,
    publisher => 'Wiktionary Export Project',
    availability => 'Creative Commons Attribution/Share-Alike',
    pub_date => "$year-$mon-$mday",
    pub_url => 'http://wiktionary-export.nataraj.su',
    series_title => 'Wiktionary dictionaries',
    source_desc => 'This dictionary were exported from Wiktionary Project (http://ru.wiktionary.org) by wiktionary-export.nataraj.su script',
    project_desc => "This dictionary comes from Wiktionary Poject (http://ru.wiktionary.org).
It were automaticly converyed into .tei fromat by Wiktionary Export Project.
To get new version of dictionary please visit http://wiktionary-export.nataraj.su 
The dictionary is updated weekly.
If you want to add new articles, of fix the translations, add them directly 
into http://ru.wiktionary.org, and they will be exported to this dictionary.
If you've noticed convertation errors, or want to join the Wiktionary Export Project,
feel free to write to n\@shaplov.ru With Wiktionary-Export note in subject.",
    );
  $tei_header->add_resp_person(
    name => "Swami Dhyan Nataraj (Nikolay Shaplov)", 
    role => "Script Author",
    email => 'dhyan@nataraj.su');

  $TEI->add_header($tei_header);
  return $TEI;
}

sub date
{
  my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
  $year+=1900;
  $mon+=1;
  $mon = '0'.$mon if length($mon)==1;
  $mday = '0'.$mday if length($mday)==1;
  return ($year,$mon,$mday);
}

sub write_log
{
  my $message = join("\t",@_);
  chomp $message;

  if ($message=~/No\ \'Перевод\'\ title\ in\ article\ on\ page/ )
  {
    open F, ">>:utf8", "output/errors_missing_translation_header.log";
  } else
  {
    open F, ">>:utf8", "output/errors.log";
  }
  print F join("\t",@_),"\n";
  close F;
}

sub escape_quotes
{
  my $s = shift;
  $s=~s/\\/\\\\/g;
  $s=~s/\"/\\"/g;
  return $s;
}
