#!/usr/bin/perl

use strict;
use MediaWiki::API;


use URI::Escape;
use utf8;
binmode STDOUT, ':utf8';

use Devel::Peek;
use Data::Dumper;

use Wiktionary::Server;
use Wiktionary::Page;


use StarDict::Writer;

my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
$year+=1900;
$mon+=1;
$mon = '0'.$mon if length($mon)==1;
$mday = '0'.$mday if length($mday)==1;
my $date = "$year-$mon-$mday";

use TEI::FreeDict;
my $tei = new TEI::FreeDict;


my $sd_writer = new StarDict::Writer ();

my $server = bless({_lang=>'ru'},'Wiktionary::Ru::Server');

open F,"commode.wiki";
my $s="";
while (my $ss= <F>)
{
  $s.=$ss;
}
use Encode;

Encode::_utf8_on($s);

my $page = new Wiktionary::Ru::Page(server=>$server, title => 'commode', body => $s);


foreach my $lang ($page->sections_langs)
{
  my $count = 1;
  open F, ">:encoding(UTF-8)",$lang;
  print F $page->get_section($lang)->{_body};
  close F;
  
#  $page->get_section($lang)->_split_on_articles;
  
  foreach ($page->get_section($lang)->articles)
  {
#    $_->_parse_src($_->{_body});
 #   print "$lang.$count \n";
    open F,  '>:encoding(UTF-8)',"$lang.$count";
    # print F $_->{_body};
    print F $_->translation_as_TEI('ru')->asString; 
        print F "============================\n";
    print F $_->translation_as_TEI('ru')->asPlainTextEntry; 
        $sd_writer->append_article($_->title."-".$_->lang, $_->translation('ru')->asPlainTextEntry);
    
    close F;
    $count ++;
  }
}
$sd_writer->write;

close (F);
