#!/usr/bin/perl

use strict;
use XML::LibXML;

# usage: ./xml_reindent.pl in.xml >out.xml

my $FileName= $ARGV[0];  
my $parser = XML::LibXML->new();
my $xml = $parser->parse_file($FileName);
my $root = $xml->getDocumentElement();
indent_as_block($root);
print $xml->toString();

sub has_nonempty_text_child
{
  my $parent = shift;
  foreach my $child ($parent->getChildNodes())
  {
    if ($child->nodeType() == XML_TEXT_NODE)
    {
      my $text = $child->getData();
      return 1 unless $text =~/^\s*$/;
    }
  }
  return 0;
}

sub indent_as_block
{
  my $node = shift;
  my $level = shift || 0;

  foreach my $child ($node->getChildNodes())
  {
    if ($child->nodeType() == XML_TEXT_NODE &&  $child->getData() =~/^\s*$/)
    {
      $node->removeChild($child);
    }
  }
  foreach my $child ($node->getChildNodes())
  {
    my $new_node=$node->getOwnerDocument()->createTextNode("\n". ("  " x ($level+1)));
    $node->insertBefore($new_node,$child);
    if (has_nonempty_text_child ($child))
    {
      indent_as_line($child);
    } else
    {
      indent_as_block($child,$level+1);
    }
  }
  if ($node->hasChildNodes)
  {
    my $new_node=$node->getOwnerDocument()->createTextNode("\n". ("  " x ($level)));
    $node->insertBefore($new_node,undef); # the last one
  }
}

sub indent_as_line
{
#  my $node = shift;
#  foreach my $child ($node->getChildNodes())
#  {
#    if ( $child->nodeType() == XML_TEXT_NODE )
#    {
#      my $text=$child->getData();
#      $text=~s/ +/ /g;
#      $child->setData($text);
#    }
#  }
}

