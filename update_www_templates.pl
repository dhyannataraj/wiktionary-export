#!/usr/bin/perl

use strict;
use utf8;

use FindBin;
use lib $FindBin::Bin."/.";


use Path::Tiny;

use Wiktionary;
use Wiktionary::Ru;
use Libredict::Ru;
use Libredict::En;


my $project_root = $FindBin::Bin;
my $do_not_edit_warning = "<!--#set var='_comment_' value='!!!!!!!!!!!!!!! This file was automatically created by update_www_templates.pl script. Do not edit it directly !!!!!!!!!!!!!!!!!!'-->";

my @any_ru_langs=qw(en fr de fi lv es it la sah chm tt ba os myv cv ce kk);


foreach my $lang (@any_ru_langs)
{
  print "=====$lang====\n";
  my $long_name = Libredict::Ru::lang_pair_name($lang,'ru');
  my $text =
"$do_not_edit_warning
<!--#set var='short_name' value='$lang-ru'-->
<!--#set var='long_name'  value='$long_name'-->
<!--#include virtual='info_tmpl.shtml' -->";
  path($project_root)->child(qw(www ru))->child("info_$lang-ru.html")->spew_utf8($text);
}

foreach my $lang (@any_ru_langs)
{
  print "=====$lang====\n";
  my $long_name = Libredict::En::lang_pair_name($lang,'ru');
  my $text =
"$do_not_edit_warning
<!--#set var='short_name' value='$lang-ru'-->
<!--#set var='long_name'  value='$long_name'-->
<!--#include virtual='info_tmpl.shtml' -->";
  path($project_root)->child(qw(www en))->child("info_$lang-ru.html")->spew_utf8($text);
}

my $text = "$do_not_edit_warning\n\n";

foreach my $lang (@any_ru_langs)
{
  print "=====$lang====\n";
  my $long_name = Libredict::Ru::lang_pair_name($lang,'ru');
  $text .= "<!--#set var='short_name' value='$lang-ru'--><!--#set var='long_name' value='$long_name'--><!--#include virtual='index_table_row.shtml' wait='yes' -->\n"
}
path($project_root)->child(qw(www ru))->child("index_any-ru_table.shtml")->spew_utf8($text);


$text = "$do_not_edit_warning\n\n";

foreach my $lang (@any_ru_langs)
{
  print "=====$lang====\n";
  my $long_name = Libredict::En::lang_pair_name($lang,'ru');
  $text .= "<!--#set var='short_name' value='$lang-ru'--><!--#set var='long_name' value='$long_name'--><!--#include virtual='index_table_row.shtml' wait='yes' -->\n"
}
path($project_root)->child(qw(www en))->child("index_any-ru_table.shtml")->spew_utf8($text);


my @langs= qw(en de fr es it uk eo pl cs nl bg pt be la fi sv tr el hr ko da lv sah chm tt ba os myv cv ce kk);

my $table_ru = "$do_not_edit_warning\n";
my $table_en = "$do_not_edit_warning\n";

foreach my $lang (@langs)
{
  my $long_name_en = Libredict::En::lang_pair_name('ru',$lang);
  my $text =
"$do_not_edit_warning
<!--#set var='short_name' value='ru-$lang'-->
<!--#set var='long_name'  value='$long_name_en'-->
<!--#include virtual='info_ru-any_tmpl.shtml' wait='yes' -->\n";

  path($project_root)->child(qw(www en))->child("info_ru-$lang.html")->spew_utf8($text);

  my $long_name_ru = Libredict::Ru::lang_pair_name('ru',$lang);
  $text =
"$do_not_edit_warning
<!--#set var='short_name' value='ru-$lang'-->
<!--#set var='long_name'  value='$long_name_ru'-->
<!--#include virtual='info_ru-any_tmpl.shtml' wait='yes' -->";

  path($project_root)->child(qw(www ru))->child("info_ru-$lang.html")->spew_utf8($text);

  $table_ru .=  "<!--#set var='short_name' value='ru-$lang'--><!--#set var='long_name' value='$long_name_ru'--><!--#include virtual='index_table_row.shtml' wait='yes' -->\n";
  $table_en .=  "<!--#set var='short_name' value='ru-$lang'--><!--#set var='long_name' value='$long_name_en'--><!--#include virtual='index_table_row.shtml' wait='yes' -->\n";
}

path($project_root)->child(qw(www ru))->child("index_ru-any_table.shtml")->spew_utf8($table_ru);
path($project_root)->child(qw(www en))->child("index_ru-any_table.shtml")->spew_utf8($table_en);
