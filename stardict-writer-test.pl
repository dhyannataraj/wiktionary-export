#!/usr/bin/perl

use strict;

use Lingua::StarDict::Writer;

my $data = [
             {title => "second", type=> "t", body => "blah-blah-blah"},
             {title => "second", type=> "h", body => "This	is	second	article"},
             {title => "third", type=> "h", body => "This\nis\nthird\narticle 2"},
             {title => "first", type=> "h", body => "This is <b>first</b> article 3"},
           ];

my $stardict_writer = new Lingua::StarDict::Writer (name=>'Test Star Dict', date=>"2020-12-31");

foreach my $entry (@$data)
{
  $stardict_writer->add_entry_part($entry->{title}, $entry);
}

$stardict_writer->write;