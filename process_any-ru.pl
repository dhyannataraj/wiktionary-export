#!/usr/bin/perl
# a script to get the russian words from a given language
# wiktionary server

use strict;
use utf8;
use FindBin;
use lib $FindBin::Bin."/.";

binmode STDOUT, ':utf8';


use File::Path qw(make_path);

use TEI::FreeDict;
use Wiktionary::Server;
use Lingua::StarDict::Writer;

# these are the names of the langaugaes of the various
# wiktionary servers. 
my %lang_names=('fr' => "French",
                'en' => "English",
                'de' => "German",
                'es' => "Spanish",
                'it' => "Italian",
                'lv' => "Latvian",
                'fi' => "Finnish",
                'la' => "Latin",
                'ltg' => "Latgalian",
                'sah' => "Yakut",
                'chm' => "Mari",
                'tt' => "Tatar",
                'ba' => "Bashkir",
                'os' => "Ossetian",
                'myv' => "Erzya",
                'cv' => "Chuvash",
                'ce' => "Chechen",
                'kk' => "Kazakh",
                );

my $from_lang = $ARGV[0];
die "Please specify lang code as a first arg" unless $from_lang;
die "Unknown lang code '$from_lang'" unless $lang_names{$from_lang};

print "Exporting ${lang_names{$from_lang}}-Russian dictionary\n";

my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
$year+=1900;
$mon+=1;
$mon = '0'.$mon if length($mon)==1;
$mday = '0'.$mday if length($mday)==1;
my $date = "$year-$mon-$mday";


my $short_name = "${from_lang}-ru";
my $long_name = "Wiktionary ".$lang_names{$from_lang}."-Russian";


open ERRORS, ">:utf8", "${short_name}_$year-$mon-$mday.errors";
open EMPTY, ">:utf8", "${short_name}_$year-$mon-$mday.empty_articles";

my $server = new Wiktionary::Server(lang=>'ru');


my @list=();
if (-e "$short_name.list.10")
{
  $server->debug(1);
  print "Using manual list\n";
  open F, "<:utf8","$short_name.list.10";
  while (my $s=<F>)
  {
    chomp $s;
    push @list, $s;
  }
  close F;
} else
{
  print "Getting word list...\n";
  @list = $server->get_word_list($from_lang);
}

my $TEI = new TEI::FreeDict;
my $stardict_writer = new Lingua::StarDict::Writer (name=>$long_name,date=>"$year-$mon-$mday");

open PLAIN, ">:utf8", "wiktionary_${short_name}_latest_plaintext.txt";

my $word_count = 0;
foreach my $word (@list)
{
  print "Processing $word...\n";
  my $page;
  my @good_articles=();
  eval{
         $page = $server->get_page($word);
         $page->get_section($from_lang)->articles;
      };
  if ($@)
  {
    my $error_msg = $@;
    print ERRORS "$word\t$@"
  } else
  {
    my @all_articles=$page->get_section($from_lang)->articles;
    foreach my $ar (@all_articles)
    {
      eval{
       $ar->translation_as_TEI('ru');
      };
      if ($@)
      {
        my $error_msg = $@;
        if ($#all_articles==0 && $error_msg =~ /Section '.*' is empty on page/)
        {
         print EMPTY "$word\n";
        } else
        {
          print ERRORS "$word\t$@" 
        }
      } else
      {
        push @good_articles, $ar;
      }
    }
  }


  if (@good_articles)
  {
    foreach my $article (@good_articles)
    {
      my $tei_entry = $article->translation_as_TEI('ru');
      $TEI->add_entry($tei_entry);
      $stardict_writer->entry($article->title)->add_part(type=> 't', data => $tei_entry->form->pronunciation) if $tei_entry->form->pronunciation;
      $stardict_writer->entry($article->title)->add_part(type=> 'm', data => $tei_entry->asPlainTextEntry({skip_headword => 1, skip_pronunciation => 1}) );

      print PLAIN $tei_entry->asPlainTextEntry,"\n";
      print PLAIN "--------------------\n";
    }
    $word_count++;
  }
}
close PLAIN;


my $tei_header = new TEI::FreeDict::Header(
    title => $long_name, 
    edition => $year*0.0001+$mon*0.000001+$mday*0.00000001,
    extent => $word_count,
    publisher => 'Wiktionary Export Project',
    availability => 'Creative Commons Attribution/Share-Alike',
    pub_date => "$year-$mon-$mday",
    pub_url => 'http://wiktionary-export.nataraj.su',
    series_title => 'Wiktionary dictionaries',
    source_desc => 'This dictionary were exported from Wiktionary Project (http://ru.wiktionary.org) by wiktionary-export.nataraj.su script',
    project_desc => "This dictionary comes from Wiktionary Poject (http://ru.wiktionary.org).
It were automaticly converyed into .tei fromat by Wiktionary Export Project.
To get new version of dictionary please visit http://wiktionary-export.nataraj.su 
The dictionary is updated weekly.
If you want to add new articles, of fix the translations, add them directly 
into http://ru.wiktionary.org, and they will be exported to this dictionary.
If you've noticed convertation errors, or want to join the Wiktionary Export Project,
feel free to write to n\@shaplov.ru With Wiktionary-Export note in subject.",
    );
$tei_header->add_resp_person(
	name => "Swami Dhyan Nataraj (Nikolay Shaplov)", 
	role => "Script Author",
	email => 'dhyan@nataraj.su');

$TEI->add_header($tei_header);
$stardict_writer->write;
open TEI, ">:utf8", "wiktionary_${short_name}_$date.tei";

print TEI $TEI->asString;
close TEI;

my $path_base = "/.www-data/nataraj/wiktionary-export/dictionaries";
die "Path '$path_base' not found, do not know where to put dictionaries" unless -d $path_base;

my $www_path = "$path_base/$from_lang-ru";

if (! (-d $www_path))
{
  make_path($www_path);
  die "Unable to create '$www_path' in dictionary storage. Aborting" unless -d $www_path;
}

my $path_logs = "$www_path/bot_messages";

if (! (-d $path_logs))
{
  make_path($path_logs);
  die "Unable to create '$path_logs' in dictionary storage. Aborting" unless -d $path_logs;
}


`gzip wiktionary_${short_name}_$date.tei`;
`mv wiktionary_${short_name}_$date.tei.gz $www_path`;

`tar -cvzf wiktionary_$from_lang-ru_stardict_$date.tgz '$long_name'`;
`mv wiktionary_$from_lang-ru_stardict_$date.tgz $www_path`;
`rm -rf '$long_name'`;

`gzip wiktionary_${short_name}_latest_plaintext.txt`;
`mv wiktionary_${short_name}_latest_plaintext.txt.gz $www_path`;


`echo "$date" >  $www_path/last_release_date`;
`echo "$word_count" >  $www_path/last_release_word_count`;

`mv ${short_name}_$date.empty_articles $path_logs/${date}_empty_articles`;

open F,"<:utf8", "${short_name}_$date.errors";

open F_MISSING, ">:utf8", "$path_logs/${date}_missing_translation_header";
open F_TOO_COMPLEX, ">:utf8", "$path_logs/${date}_too_complex";
open F_OTHER, ">:utf8", "$path_logs/${date}_other_errors";
open F_WRONG_FORMATTING, ">:utf8", "$path_logs/${date}_wrong_formatting";
while (my $s=<F>)
{
  if ($s=~/No\ \'Значение\'\ title\ in\ article\ on\ page/)
  {
    print F_MISSING $s;
  } elsif ( ($s=~/More\ than\ one\ \'Значение\'\ title\ in\ article\ on\ page/) ||
            ($s=~/No\ \'\#\'\ at\ the\ begining\ of\ the\ line\ in\ ordered\ list\ on\ page/)
          )
  {
    print F_TOO_COMPLEX $s;
  } elsif ($s=~/Some\ garbige\ \(.*?\)\ at\ the\ end\ of\ section\ title/)
  {
    print F_WRONG_FORMATTING $s;
  }
   else
  {
    print F_OTHER $s;
  }
}

close F_MISSING;
close F_OTHER;
close F_TOO_COMPLEX;
close F_WRONG_FORMATTING;
close F;

`rm ${short_name}_$date.errors`;

